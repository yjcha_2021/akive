let globalEventBus = new Vue();

let sdpVueGlobalEvent = {
    paging:{
        rowCount:null
    }
};

let pagingComponent = {
    template: '<div class="paging mt10"> <a class="prev" @click="goPaging(pageIndex === 1 ? 1 : pageIndex - 1)" style="cursor: pointer;">이전</a> <template v-for="currentPage in pagingEndNum" v-if="currentPage >= pagingStartNum"><a :class="pageIndex == currentPage ? \'on\' : \'\'" @click="goPaging(currentPage)" style="cursor: pointer;">{{currentPage}}</a>{{ \' \'}}</template><a class="next" @click="goPaging(pageIndex === totalPageSize ? pageIndex : pageIndex + 1)" style="cursor: pointer;">다음</a> </div>',
    name: "paging-component",
    props: {
        pagingData: Object
    },
    data: function() {
        return {
            pageIndex: null,
            pageRowSize: null,
            pageStartIndex: null,
            totalCount: null,
            pagingStartNum: null,
            pagingEndNum: null,
            totalPageSize: null
        };
    },
    watch: {
        pagingData: function() {
            let pagingCurrentIndex = this.pagingData.option.pagingCurrentIndex; //현재 페이지
            let pagingNavigatorCount = this.pagingData.option.pagingNavigatorCount; //페이징 넘버 노출 개수
            let pagingRowCount = this.pagingData.option.pagingRowCount; //리스트 개수
            let totalCount = this.pagingData.totalCount; //리스트 전체 개수

            let pagingVariable = Math.floor((pagingCurrentIndex - 1) / pagingNavigatorCount) * pagingNavigatorCount;
            let totalPageSize = Math.ceil(totalCount / pagingRowCount);
            let pagingStartNum = pagingVariable + 1;
            let pagingEndNum = pagingVariable + pagingNavigatorCount > totalPageSize ? totalPageSize : pagingVariable + pagingNavigatorCount;

            this.$set(this, "pageIndex", pagingCurrentIndex);
            this.$set(this, "pageRowSize", pagingRowCount);
            this.$set(this, "totalCount", totalCount);
            this.$set(this, "pagingStartNum", pagingStartNum);
            this.$set(this, "pagingEndNum", pagingEndNum);
            this.$set(this, "totalPageSize", totalPageSize);

            globalEventBus.$emit(sdpVueGlobalEvent.paging.rowCount, pagingRowCount);

        }
    },
    mounted: function() {
    },
    methods: {
        goPaging: function(selectedPage) {
            this.$emit("go-paging", selectedPage);
        }
    }
};

let commonMixinData = {
    data: function() {
        return {
            list:[],
            optionalData:new Object(),
            totalCount: 0,
            ajaxResultData: null,
            url:null,
            searchData:new Object(),
            searchOption:new Object(),
            originSearchData:new Object(),
            pagingCurrentIndex:1,
            pagingRowCount:null
        };
    },
    mounted: function() {
        globalEventBus.$on(sdpVueGlobalEvent.paging.rowCount, $.proxy(function (row) {
            this.$set(this, "pagingRowCount", row);
        }, this));
    },
};

let commonMixinMethods = {
    methods: {
        setCallAjax: function(url, searchData, searchOption) {
            if(!this.isEmpty(url)) {
                this.$set(this, "url", url);
            }

            if(!this.isEmpty(searchData)) {
                this.$set(this, "searchData", searchData);
                this.$set(this, "originSearchData", JSON.parse(JSON.stringify(searchData)));
            }

            if(!this.isEmpty(searchOption)) {
                this.$set(this, "searchOption", searchOption);
            }

            this.resetPagingCurrentIndex();
            this.doCallAjax();
        },
        doCallAjax: function() {
            $.ajax({
                type: "POST",
                contentType:"application/json",
                dataType:"json",
                url: this.url,
                data: JSON.stringify(this.getRequestData()),
                success:(data) => {
                    if(data.resultCode === 200) {

                        for(let search in this.searchData) {
                            this.$set(this.searchData, search, data.option.searchData[search]);
                            this.$set(this.originSearchData, search, data.option.searchData[search]);
                        }

                        this.$set(this, "ajaxResultData", data);
                        this.$set(this, "list", data.data);
                        this.$set(this, "optionalData", data.optionalData);
                        this.$set(this, "totalCount", data.totalCount);
                    } else {
                        console.log("error");
                    }
                }
            });

        },
        getRequestData: function() {
            let requestData = {
                pagingCurrentIndex : this.pagingCurrentIndex,
                pagingRowCount : this.pagingRowCount
            };

            return $.extend(true, new Object(), requestData, {searchData : this.searchData}, this.searchOption);
        },
        goPaging: function(selectedPage) {
            this.$set(this, "pagingCurrentIndex", selectedPage);
            this.setSearchData();
            this.doCallAjax();
        },
        doSearch: function() {
            this.resetPagingCurrentIndex();
            this.doCallAjax();
        },
        setPagingRow: function() {
            this.resetPagingCurrentIndex();
            this.setSearchData();
            this.doCallAjax();
        },
        resetPagingCurrentIndex: function() {
            this.$set(this, "pagingCurrentIndex", 1);
        },
        prevPagingCurrentIndex: function() {
            this.setSearchData();
            if(this.pagingCurrentIndex !== 1) {
                this.$set(this, "pagingCurrentIndex", this.pagingCurrentIndex - 1);
            }
        },
        setSearchData: function() {
            for(let search in this.originSearchData) {
                this.$set(this.searchData, search, this.originSearchData[search]);
            }
        },
        isEmpty: function(obj) {
            if(obj == null || obj == undefined || obj === "") {
                return true;
            } else {
                return false;
            }
        },
        getUUID:function() {
            return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        }
    }
};

let commonMixinFilters = {
    filters:{
        formatNumber:function(num) {
            if(!num) {
                return "0";
            } else {
                return String(num).replace(/(\d)(?=(?:\d{3})+(?!\d))/g, "$1,");
            }
        },
        formatDate:function(date, type) {
            if(!date) {
                return "-";
            } else {
                if(!type) {
                    return moment(date).format("YYYY-MM-DD HH:mm:ss");
                } else {
                    if(type.toLowerCase() == "date") {
                        return moment(date).format("YYYY-MM-DD");
                    } else if(type.toLowerCase() == "time") {
                        return moment(date).format("HH:mm:ss");
                    } else {
                        return moment(date).format("YYYY-MM-DD HH:mm:ss");
                    }
                }
            }
        },
        formatPhoneNumber:function(phoneNum) {
            if(!phoneNum) {
                return "-";
            } else {
                return String(phoneNum).replace(/(^01[016789]{1}|02|0[3-9]{1}[0-9]{1})([*0-9]+)?([*0-9]{4})$/,"$1-$2-$3");
            }
        },
        formatNullValue:function(value) {
            if(!value) {
                return "-";
            } else {
                return value;
            }
        }
    }
};
