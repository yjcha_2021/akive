/****/
$(function(){
	/** POP UP **/
	$('.pop_btn').click(function() {
		var popCon = $(this).attr("href");
		var popLeft = (($(window).width() - $(popCon).outerWidth()) / 2);
		var popTop = (($(window).height() - $(popCon).outerHeight()) / 2 );
		if(popLeft < 0) popLeft = 0;
		if(popTop < 0) popTop = 0;
		$('body').addClass('popup_visible');
		$('.popup_mask').fadeIn();
		$(popCon).css({"left":popLeft, "top":popTop}).fadeIn().addClass('on');
		return false;
	});
	$(window).resize(function() {
		var popCon = $(".popup.on");
		var popLeft = (($(window).width() - $(popCon).outerWidth()) / 2);
		var popTop = (($(window).height() - $(popCon).outerHeight()) / 2 );
		if(popLeft < 0) popLeft = 0;
		if(popTop < 0) popTop = 0;
		$(popCon).css({"left":popLeft, "top":popTop});
	});
	$('.popup .pop_close').click(function() {
		$(this).closest('.popup').fadeOut().removeClass('on');
		$('body').removeClass('popup_visible');
		$('.popup_mask').fadeOut();
		return false;
	});
	$('.popup_mask').click(function() {
		$('.popup').fadeOut().removeClass('on');
		$('.popup_mask').fadeOut();
		$('body').removeClass('popup_visible');
		return false;
	});
});

/** POP UP **/
function pop_close(obj){
	$(obj).fadeOut().removeClass('on');
	$('body').removeClass('popup_visible');
	$('.popup_mask').fadeOut();
}
function pop_open(obj){
	var popLeft = (($(window).width() - $(obj).outerWidth()) / 2);
	var popTop = (($(window).height() - $(obj).outerHeight()) / 2 );
	if(popLeft < 0) popLeft = 0;
	if(popTop < 0) popTop = 0;
	$('body').addClass('popup_visible');
	$('.popup_mask').fadeIn();
	$(obj).css({"left":popLeft, "top":popTop}).fadeIn().addClass('on');
	return false;
}
