;(($) => {
	$.ajaxSetup({
		beforeSend:function(xhr) {
			xhr.setRequestHeader("AJAX", "true");
		},
		complete:function() {
		},
		error:function(xhr) {
			if(9000 === xhr["responseJSON"]["resultCode"]) {
				try {
					alert(xhr["responseJSON"]["resultMessage"]);
				} finally {
					// TODO : session 만료 시, redirect 될 로그인 페이지 URI
					location.href = "/dev/login";
				}
			}
		}
	});
})($);
