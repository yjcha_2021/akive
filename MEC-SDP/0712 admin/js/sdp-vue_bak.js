let globalEventBus = new Vue();

let sdpVueGlobalEvent = {
    paging:{
        rowCount:null
    }
};

let pagingComponent = {
    template: '<div class="paginate" v-if="totalCount > pageRowSize"><a class="first" style="cursor: pointer;" v-on:click="goPaging(1)">처음 페이지</a><a class="prev" style="cursor: pointer;" v-on:click="goPaging(pageIndex - 1 < 1 ? 1 : pageIndex - 1)">이전 블록 페이지</a><ul><li v-bind:class="pageIndex == currentPage ? \'select\' : \'\'" v-for="currentPage in pagingEndNum" v-if="currentPage >= pagingStartNum"><a style="cursor: pointer;" v-on:click="goPaging(currentPage)">{{currentPage}}</a></li></ul><a class="next" style="cursor: pointer;" v-on:click="goPaging(pageIndex + 1 > totalPageSize ? totalPageSize : pageIndex + 1)">다음 블록 페이지</a><a class="last" style="cursor: pointer;" v-on:click="goPaging(totalPageSize)">마지막 페이지</a></div>',
    name: "paging-component",
    props: {
        pagingData: Object
    },
    data: function() {
        return {
            pageIndex: null,
            pageRowSize: null,
            //pageSize: null,
            pageStartIndex: null,
            totalCount: null,
            pagingStartNum: null,
            pagingEndNum: null,
            totalPageSize: null
        };
    },
    watch: {
        pagingData: function() {
            let pagingCurrentIndex = this.pagingData.option.pagingCurrentIndex; //현재페이지
            let pagingNavigatorCount = this.pagingData.option.pagingNavigatorCount; //페이지 노출 수
            let pagingRowCount = this.pagingData.option.pagingRowCount; //리스트 수
            let totalCount = this.pagingData.totalCount; //전체 수

            let pagingVariable = Math.floor((pagingCurrentIndex - 1) / pagingNavigatorCount) * pagingNavigatorCount; //
            let totalPageSize = Math.ceil(totalCount / pagingNavigatorCount);
            let pagingStartNum = pagingVariable + 1;
            let pagingEndNum = pagingVariable + pagingNavigatorCount > totalPageSize ? totalPageSize : pagingVariable + pagingNavigatorCount;

            this.$set(this, "pageIndex", pagingCurrentIndex);
            this.$set(this, "pageRowSize", pagingRowCount);
            this.$set(this, "totalCount", totalCount);
            this.$set(this, "pagingStartNum", pagingStartNum);
            this.$set(this, "pagingEndNum", pagingEndNum);
            this.$set(this, "totalPageSize", totalPageSize);

            globalEventBus.$emit(sdpVueGlobalEvent.paging.rowCount, pagingRowCount);

        }
    },
    mounted: function() {
    },
    methods: {
        goPaging: function(selectedPage) {
            this.$emit("go-paging", selectedPage);
        }
    }
};

let commonMixinData = {
    data: function() {
        return {
            list:[],
            optionalData:new Object(),
            totalCount: 0,
            ajaxResultData: null,
            url:null,
            optionalSearchData:new Object(),
            pagingCurrentIndex:1,
            pagingRowCount:null,
            searchType:"",
            searchText:""
        };
    },
    mounted: function() {
        globalEventBus.$on(sdpVueGlobalEvent.paging.rowCount, $.proxy(function (row) {
            this.$set(this, "pagingRowCount", row);
        }, this));
    },
};

let commonMixinMethods = {
    methods: {
        doCallAjax: function(url, optionalSearchData) {
            if(!this.isEmpty(url)) {
                this.$set(this, "url", url);
            }

            if(!this.isEmpty(optionalSearchData)) {
                this.resetPagingCurrentIndex();
                this.$set(this, "optionalSearchData", optionalSearchData);
            }

            $.ajax({
                type: "POST",
                contentType:"application/json",
                dataType:"json",
                url: this.url,
                data: JSON.stringify(this.getRequestData()),
                success:(data) => {
                    if(data.resultCode === 200) {
                        console.log(data)
                        this.$set(this, "ajaxResultData", data);
                        this.$set(this, "list", data.data);
                        this.$set(this, "optionalData", data.optionalData);
                        this.$set(this, "totalCount", data.totalCount);
                    } else {
                        console.log("error");
                    }
                }
            });

        },
        getRequestData: function() {
            let requestData = {
                pagingCurrentIndex : this.pagingCurrentIndex,
                pagingRowCount : this.pagingRowCount,
                searchType : this.searchType,
                searchText : this.searchText,
            };
            return $.extend(true, new Object(), requestData, this.optionalSearchData);
        },
        goPaging: function(selectedPage) {
            this.$set(this, "pagingCurrentIndex", selectedPage);
            this.doCallAjax();
        },
        doSearch: function() {
            if(this.isEmpty(this.searchType) && !this.isEmpty(this.searchText)) {
                alert("검색구분을 선택하세요.");
            } else {
                this.resetPagingCurrentIndex();
                this.doCallAjax();
            }

        },
        setPagingRow: function() {
            this.resetPagingCurrentIndex();
            this.doCallAjax();
        },
        resetPagingCurrentIndex: function() {
            this.$set(this, "pagingCurrentIndex", 1);
        },
        isEmpty: function(obj) {
            if(obj == null || obj == undefined || obj === "") {
                return true;
            } else {
                return false;
            }
        }
    }
};

let commonMixinFilters = {
    filters:{
    }
};