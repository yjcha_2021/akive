/****/
$(function(){

	/** gnb **/
	$('#gnb > ul > li').on("mouseover focusin", function(){
		$(this).addClass("on");
		$(this).children('ul').stop().slideDown();
	});
	$('#gnb > ul > li').on("mouseout focusout", function(){
		$(this).removeClass("on");
		$(this).children('ul').stop().slideUp();
	});


	/** Lnb **/
	/*lnb_dep = $('.lnb_nav').outerHeight() - ($('.lnb_nav > li > a').outerHeight() * $('.lnb_nav > li').length);
	$('.lnb_dep').height(lnb_dep);
	$('.lnb_dep .scrollbar-inner').height(lnb_dep);
	$(window).resize(function() {
		lnb_dep = $('.lnb_nav').outerHeight() - ($('.lnb_nav > li > a').outerHeight() * $('.lnb_nav > li').length);
		$('.lnb_dep').height(lnb_dep);
		$('.lnb_dep .scrollbar-inner').height(lnb_dep);
	});
	$('.lnb_nav > li:first-child').find('.lnb_dep').slideDown(function(){
		$('.lnb_nav > li:first-child').addClass('on');
	});
	$('.lnb_nav > li > a').click(function(){
		if(!$(this).parent('li').hasClass('banner')){
			if (!$(this).parent('li').hasClass('on')) {
				$(this).closest('.lnb_nav').find('.lnb_dep').slideUp(function(){
					$(this).closest('.lnb_nav').find('li').removeClass('on');
				});
				$(this).next('.lnb_dep').slideDown(function(){
					$(this).parent('li').addClass('on');
				});
			}
		} else {
			var link = $(this).attr("href");
			location.href=link;
		}
		return false;
	});*/


	/** dep_sub **/
	/*$('.dep_sub > li:first-child').find('.dep_sub_area').slideDown(function(){
		$('.dep_sub > li:first-child').addClass('on');
	});

	$('.dep_sub > li > a').click(function(){
		if (!$(this).parent('li').hasClass('on')) {
			$(this).closest('.dep_sub').find('.dep_sub_area').slideUp(function(){
				$(this).closest('.dep_sub').find('li').removeClass('on');
			});
			$(this).next('.dep_sub_area').slideDown(function(){
				$(this).parent('li').addClass('on');
			});
		}
		return false;
	});*/


	/** tab **/
	$('.tab_nav a').click(function(){
		$(this).closest('.tab_nav').find('li').removeClass('on');
		$(this).parent('li').addClass('on');
		var tCon = $(this).attr("href");
		$(tCon).parents().find('.tab_con').removeClass('on');
		$(tCon).addClass('on');
		return false;
	});
	/** tab02 **/
	$('.tab_nav_inn a').click(function(){
		$(this).closest('.tab_nav_inn').find('li').removeClass('on');
		$(this).parent('li').addClass('on');
		var tCon = $(this).attr("href");
		$(tCon).parent().find('.tab_con_inn').removeClass('on');
		$(tCon).addClass('on');
		return false;
	});


	/** POP UP **/
	$('.pop_btn').click(function() {
		var popCon = $(this).attr("href");
		var popLeft = (($(window).width() - $(popCon).outerWidth()) / 2);
		var popTop = (($(window).height() - $(popCon).outerHeight()) / 2 );
		if(popLeft < 0) popLeft = 0;
		if(popTop < 0) popTop = 0;
		$('body').addClass('popup_visible');
		$('.popup_mask').fadeIn();
		$(popCon).css({"left":popLeft, "top":popTop}).fadeIn().addClass('on');
		return false;
	});
	$(window).resize(function() {
		var popCon = $(".popup.on");
		var popLeft = (($(window).width() - $(popCon).outerWidth()) / 2);
		var popTop = (($(window).height() - $(popCon).outerHeight()) / 2 );
		if(popLeft < 0) popLeft = 0;
		if(popTop < 0) popTop = 0;
		$(popCon).css({"left":popLeft, "top":popTop});
	});
	$('.popup .pop_close').click(function() {
		$(this).closest('.popup').fadeOut().removeClass('on');
		$('body').removeClass('popup_visible');
		$('.popup_mask').fadeOut();
		return false;
	});
	$('.popup_mask').click(function() {
		$('.popup').fadeOut().removeClass('on');
		$('.popup_mask').fadeOut();
		$('body').removeClass('popup_visible');
		return false;
	});


	/*** Layout ***/
	/*$(window).on("load",function(){
		//$(".contents").mCustomScrollbar({axis:"yx",theme:"minimal-dark",scrollInertia:300});
		$(".scrollbar-inner").mCustomScrollbar({theme:"minimal-dark",scrollInertia:300});
		$(".table_scroll").mCustomScrollbar({axis:"x",theme:"minimal-dark",scrollInertia:300});
		$(".scroll_all").mCustomScrollbar({axis:"xy",theme:"minimal-dark",scrollInertia:300,advanced:{autoExpandHorizontalScroll:true}});
		$(".scroll_x").mCustomScrollbar({axis:"x",theme:"minimal-dark",scrollInertia:300});
		//$(".scroll_y").mCustomScrollbar({theme:"minimal-dark",scrollInertia:300});
		$(".scroll_y").mCustomScrollbar({theme:"minimal-dark",scrollInertia:300, callbacks:{
		   onUpdate: function() {

			if ($('#site_monitoring #mCSB_2_scrollbar_vertical').is(':visible') == true) {
			 $('.site_monitoring').addClass('pd');
			} else {
			 $('.site_monitoring').removeClass('pd');
			}

			if ($('#lm_scroll #mCSB_1_scrollbar_vertical').is(':visible') == true) {
			 $('#lm_scroll').addClass('pd');
			} else {
			 $('#lm_scroll').removeClass('pd');
			}

		   }
		  }});
	});*/


	/*** datepicker ***/
	// $(".select_date").datepicker({
	// 	monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
	// 	dayNames: [ "일", "월", "화", "수", "목", "금", "토" ],
	// 	dayNamesShort: [ "일", "월", "화", "수", "목", "금", "토" ],
	// 	dayNamesMin: [ "일", "월", "화", "수", "목", "금", "토" ],
	// 	dateFormat: 'yy년 mm월 dd일',
	// 	prevText: "이전 달",
	// 	nextText: "다음 달",
	// 	showOn: "both",
	// 	buttonText: "달력보기",
	// });


	/* timeChoice */
	function timeChoice(){
		$(function(){

			//시간선택
			$('.select_time').bind('focusin ',function(){
				var h = $(this).position().left;

				$('.ly_time').removeClass('on');
				$(this).parent().find('.ly_time').addClass('on');
				if($(this).closest('.time_list').parent('ul').hasClass('center')){
					$(this).parent().find('.ly_time').css('left', h + 82);
				}else{
					$(this).parent().find('.ly_time').css('left', h + 82);
				}

			});

			$('.ly_time').bind('mouseleave', function(){
				$(this).removeClass('on');
			});

			$('.select_date,.ui-datepicker-trigger').bind('focusin ',function(){
				if($(this).css(".ui-datepicker") == "block"){
					$('.ly_time').removeClass('on');
				} else {
					$('.ly_time').removeClass('on');
				}
			});

		});
	}
	$(function(){
		timeChoice();
	});


	//skip link list
	$('.skip_area a').click(function () {
		if($(".skip_area .list").css("display") == "none"){
			jQuery('.skip_area .list').show();
		} else {
			jQuery('.skip_area .list').hide();
		}
	});

	//topology_canvas
	$('#wrap').each(function(){
		if($('#lnb').hasClass('lnb_fold')) {
			$(this).addClass("desgin");
		}
	});

	 $(".lnb_fold > .btn_act").click(function(){
		$("#wrap").toggleClass('slid_fold');
		$(this).toggleClass('on');
	});
	 $(".palette > .btn_act").click(function(){
		$("#wrap").toggleClass('slide_palette');
		$(this).toggleClass('on');
	});
	 $(".ds_con_side > .btn_act").click(function(){
		$("#wrap").toggleClass('slide_palette');
		$(this).toggleClass('on');
	});
	  $(".popcont_dbEMS .btn_co08").click(function(){
		$(this).toggleClass('on');
	});
	$('footer').each(function(){
		if($('#contents_wrap').hasClass('design_wrap')) {
			$(this).addClass("footer02");
		}
	});

    /*$('footer').each(function(){
        if($('#contents_wrap').hasClass('plte')) {
            $(this).addClass("footer02");
        }
    });*/

	//MONITORING WRAP
	$('#wrap').each(function(){
		if($('#lnb').hasClass('lnb_ns')) {
			$('footer').addClass("footer03");
		}
	});

	$('#wrap').each(function(){
		if($('#lnb').hasClass('lnb_ns')) {
			$(this).addClass("monitoring");
		}
	});


});

/** POP UP **/
function pop_close(obj){
	$(obj).fadeOut().removeClass('on');
	$('body').removeClass('popup_visible');
	$('.popup_mask').fadeOut();
}
function pop_open(obj){
	var popLeft = (($(window).width() - $(obj).outerWidth()) / 2);
	var popTop = (($(window).height() - $(obj).outerHeight()) / 2 );
	if(popLeft < 0) popLeft = 0;
	if(popTop < 0) popTop = 0;
	$('body').addClass('popup_visible');
	$('.popup_mask').fadeIn();
	$(obj).css({"left":popLeft, "top":popTop}).fadeIn().addClass('on');
	return false;
}
