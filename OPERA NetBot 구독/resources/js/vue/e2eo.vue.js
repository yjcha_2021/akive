//검색 form을 생성하는 component
Vue.component("form-component", {
	data:function() {
		return {
			submitType:null,
			orderTarget:null,
			orderType:null,
			formAction:"",
			formSchema:null
		};
	},
	updated:function() {
		this.$nextTick(function() {
			if("search" == this.submitType) {
				this.$emit("do-call-ajax");
			} else if("move" == this.submitType) {
				$("form[name=dynamicForm]").submit();
			}
		});
	}
});

//검색영역 component
Vue.component("search-component", {
	data:function() {
		return {
			searchGroup:[]
		};
	},
	created:function() {
		this.addSearchRow();
	},
	mounted:function() {
		this.$nextTick(function() {
			this.searchGroup[0].searchDelimiter = "or";
		});
	},
	updated:function() {
		$.each(this.searchGroup, $.proxy(function(i, group) {
			if(i == 0) {
				group.addButtonShow = true;
				group.removeButtonShow = false;
			} else {
				group.addButtonShow = true;
				group.removeButtonShow = true;
			}
		}, this));
	},
	methods:{
		addSearchRow:function() {
			this.searchGroup.push({
				searchType:"",
				searchDelimiter:"or",
				searchText:"",
				addButtonShow:true,
				removeButtonShow:false,
				placeholderText:"검색어를 입력해 주세요."
			});
		},
		removeSearchRow:function(rowIndex) {
			this.searchGroup.splice(rowIndex, 1);
			this.$set(this, "searchGroup", this.searchGroup);
		},
		doSearch:function() {
			this.$emit("do-search");
		}
	}
});

//optional data를 관리하는 component
Vue.component("optional-component", {
	props:{
		optionalData:Object
	},
	data:function() {
		return {
			optional:null
		};
	},
	watch:{
		optionalData:function() {
			this.$set(this, "optional", this.optionalData.optionalData);
		}
	},
	methods:{
		doSearch:function() {
			this.$emit("do-search");
		}
	}
});

//페이징 component
Vue.component("paging-component", {
	props:{
		pagingData:Object
	},
	data:function() {
		return {
			pageIndex:null,
			pageRowSize:null,
			pageSize:null,
			pageStartIndex:null,
			totalCount:null,
			pagingStartNum:null,
			pagingEndNum:null,
			totalPageSize:null
		};
	},
	watch:{
		pagingData:function() {
			var pageIndex = this.pagingData.commonReqModel.pageIndex;
			var pageRowSize = this.pagingData.commonReqModel.pageRowSize;
			var pageSize = this.pagingData.commonReqModel.pageSize;
			var pageStartIndex = this.pagingData.commonReqModel.pageStartIndex;
			var totalCount = this.pagingData.totalCount;

			var pagingVariable = Math.floor((pageIndex - 1) / pageSize) * pageSize;
			var totalPageSize = Math.ceil(totalCount / pageRowSize);
			var pagingStartNum = pagingVariable + 1;
			var pagingEndNum = pagingVariable + pageSize > totalPageSize ? totalPageSize : pagingVariable + pageSize;

			this.$set(this, "pageIndex", pageIndex);
			this.$set(this, "pageRowSize", pageRowSize);
			this.$set(this, "pageSize", pageSize);
			this.$set(this, "pageStartIndex", pageStartIndex);
			this.$set(this, "totalCount", totalCount);
			this.$set(this, "pagingStartNum", pagingStartNum);
			this.$set(this, "pagingEndNum", pagingEndNum);
			this.$set(this, "totalPageSize", totalPageSize);
		}
	},
	methods:{
		goPaging:function(selectedPage) {
			this.$emit("go-paging", selectedPage);
		}
	}
});

var commonMixinFilters = {
	filters:{
		formatDate:function(date, type) {
			if(e2eo.isEmpty(date)) {
				return "-";
			} else {
				if(e2eo.isEmpty(type)) {
					return moment(date).format("YYYY-MM-DD HH:mm:ss");
				} else {
					if(type.toLowerCase() == "date") {
						return moment(date).format("YYYY-MM-DD");
					} else if(type.toLowerCase() == "time") {
						return moment(date).format("HH:mm:ss");
					} else {
						return moment(date).format("YYYY-MM-DD HH:mm:ss");
					}
				}
			}
		},
		rowIndex:function(index) {
			return e2eoListVue.$refs.pagingComponent.totalCount - (((e2eoListVue.$refs.pagingComponent.pageIndex - 1) * e2eoListVue.$refs.pagingComponent.pageRowSize) + index);
		},
		addComma:function(item) {
			return String(item).replace(/(\d)(?=(?:\d{3})+(?!\d))/g, "$1,");
		}
	}
};

var commonMixinMethods = {
	methods:{
		goPageMove:function(action, fieldName, fieldValue) {
			var searchData = e2eoListVue.getRequestData();
			var requestData = new Object();

			if(!e2eo.isEmpty(fieldName) && !e2eo.isEmpty(fieldValue)) {
				requestData[fieldName] = fieldValue;
			}

			Vue.set(e2eoListVue.$refs.formComponent, "submitType", "move");
			Vue.set(e2eoListVue.$refs.formComponent, "formAction", action);
			Vue.set(e2eoListVue.$refs.formComponent, "orderTarget", null);
			Vue.set(e2eoListVue.$refs.formComponent, "orderType", null);
			Vue.set(e2eoListVue.$refs.formComponent, "formSchema", $.extend(true, new Object(), searchData, requestData));
		},
		goPageMoveObject:function(action, field) {
			var searchData = e2eoListVue.getRequestData();
			var requestData = new Object();

			if (!e2eo.isEmpty(field)) {
				console.log(field);
				requestData = field;
			}

			Vue.set(e2eoListVue.$refs.formComponent, "submitType", "move");
			Vue.set(e2eoListVue.$refs.formComponent, "formAction", action);
			Vue.set(e2eoListVue.$refs.formComponent, "orderTarget", null);
			Vue.set(e2eoListVue.$refs.formComponent, "orderType", null);
			Vue.set(e2eoListVue.$refs.formComponent, "formSchema", $.extend(true, new Object(), searchData, requestData));
		},
		doChangeSort:function(orderTarget, orderType) {
			Vue.set(e2eoListVue.$refs.formComponent, "submitType", "search");
			Vue.set(e2eoListVue.$refs.formComponent, "orderTarget", orderTarget);
			Vue.set(e2eoListVue.$refs.formComponent, "orderType", orderType);
			Vue.set(e2eoListVue.$refs.formComponent, "formSchema", e2eoListVue.getRequestData());
		},
		doCheckRow:function(event) {
            if($(event.target)[0].type != "checkbox"){
                var checkbox = $(event.target).parent().find("input[type=checkbox]");
                $(checkbox).prop("checked", !$(checkbox).prop("checked"));
            }
		},
        doClickRow:function(event) {
            if($(event.target)[0].type != "radio"){
                var checkbox = $(event.target).parent().find("input[type=radio]");
                $(checkbox).prop("checked", true);
            }
        }
	}
};

var e2eoVueGlobalEvent = {
	draw2d:{
		exportComponents:"exportComponents",
		nestedComponentInfo:"nestedComponentInfo",
		nestedSapInfo:"nestedSapInfo",
		vnfComponentInfo:"vnfComponentInfo"
	}
};

var globalEventBus = new Vue();

var e2eoListVue;
var e2eoVue = e2eoVue || (function() {
	return {
		initVue:function(ajaxUrl, localComponents) {
			e2eoListVue = new Vue({
				el:"#container",
				data:{
					ajaxResultData:null,
					totalCount:0,
					pagingData:{
						pageIndex:null,
						pageRowSize:null,
						pageSize:null,
						pageStartIndex:null
					}
				},
				mounted:function() {
					this.$nextTick(function() {
						Vue.set(e2eoListVue.$refs.formComponent, "submitType", "search");
						Vue.set(e2eoListVue.$refs.formComponent, "formSchema", this.getRequestData());
					});
				},
				components:$.extend(true, new Object(), localComponents),
				methods:{
					doCallAjax:function() {
						$.ajax({
							type:"POST",
							url:ajaxUrl,
							data:$("form[name=dynamicForm]").serialize(),
							success:function(data) {
								if(data.resultCode == 200) {
									e2eoListVue.$set(e2eoListVue, "ajaxResultData", data);
									e2eoListVue.$set(e2eoListVue, "totalCount", data.totalCount);

									if(!e2eo.isEmpty(data.commonReqModel)) {
										e2eoListVue.$set(e2eoListVue.pagingData, "pageIndex", data.commonReqModel.pageIndex);
										e2eoListVue.$set(e2eoListVue.pagingData, "pageRowSize", data.commonReqModel.pageRowSize);
										e2eoListVue.$set(e2eoListVue.pagingData, "pageSize", data.commonReqModel.pageSize);
										e2eoListVue.$set(e2eoListVue.pagingData, "pageStartIndex", data.commonReqModel.pageStartIndex);
									}
								} else {
									alert(data.resultMessage);
								}
							},
							error:function(xhr) {
								console.log(xhr);
							}
						});
					},
					getRequestData:function() {
						var dynamicSearchData = new Object();
						var otherSearchData = new Object();
						var optionalSearchData = new Object();

						if(!e2eo.isEmpty(e2eoListVue.$refs.searchComponent)) {
							//dynamic search data
							dynamicSearchData = {
								dynamicSearchData:e2eoListVue.$refs.searchComponent.searchGroup
							};

							//search component other search data
							var otherSearchDataNames = new Array();
							$.each($(e2eoListVue.$refs.searchComponent.$el).find("input, select, textarea"), $.proxy(function(i, target) {
								if(!e2eo.isEmpty($(target).get(0).name)
										&& $(target).get(0).name != "searchType"
										&& $(target).get(0).name != "searchDelimiter"
										&& $(target).get(0).name != "searchText"
										&& $(target).get(0).name != "orderTarget"
										&& $(target).get(0).name != "orderType") {
									if($.inArray($(target).get(0).name, otherSearchDataNames) < 0) {
										otherSearchDataNames.push($(target).get(0).name);
									}
								}
							}, this));

							$.each(otherSearchDataNames, $.proxy(function(i, name) {
								var target = $(e2eoListVue.$refs.searchComponent.$el).find("[name=" + name + "]");
								if($(target).length > 1) {
									var targetArray = new Array();
									for(var k = 0; k < $(target).length; k++) {
										if($(target).get(k).checked) {
											targetArray.push($(target).get(k).value);
										}
									}
									otherSearchData[$(target).get(0).name] = targetArray;
								} else {
									otherSearchData[$(target).get(0).name] = $(target).val();
								}
							}, this));
						}

						if(!e2eo.isEmpty(e2eoListVue.$refs.optionalComponent)) {
							var optionalSearchDataNames = new Array();

							//optional search data
							$.each($(e2eoListVue.$refs.optionalComponent.$el).find("input, select, textarea"), $.proxy(function(i, target) {
								if(!e2eo.isEmpty($(target).get(0).name) && $.inArray($(target).get(0).name, optionalSearchDataNames) < 0) {
									optionalSearchDataNames.push($(target).get(0).name);
								}
							}, this));

							$.each(optionalSearchDataNames, $.proxy(function(i, name) {
								var target = $(e2eoListVue.$refs.optionalComponent.$el).find("[name=" + name + "]");
								if($(target).length > 1) {
									var targetArray = new Array();
									for(var k = 0; k < $(target).length; k++) {
										if($(target).get(k).checked) {
											targetArray.push($(target).get(k).value);
										}
									}
									optionalSearchData[$(target).get(0).name] = targetArray;
								} else {
									optionalSearchData[$(target).get(0).name] = $(target).val();
								}
							}, this));
						}

						//merge search data
						var requestData = $.extend(true, new Object(), dynamicSearchData, otherSearchData, optionalSearchData, this.pagingData);

						return requestData;
					},
					doSearch:function() {
						var isSeach = true;
                        $.each(e2eoListVue.$refs.searchComponent.searchGroup, $.proxy(function(i, searchData) {
                            if(!e2eo.isEmpty(searchData.searchText)) {
                            	if(e2eo.isEmpty(searchData.searchType)){
                                    openAlertModal("검색", "검색항목을 선택해주세요.", null, "확인");
                                    isSeach = false;
								} else if(e2eo.isEmpty(searchData.searchDelimiter)){
                                    openAlertModal("검색", "검색조건을 선택해주세요.", null, "확인");
                                    isSeach = false;
								}
                            }
                        }, this));

						if(isSeach){
                            this.$set(this.pagingData, "pageIndex", 1);

                            Vue.set(e2eoListVue.$refs.formComponent, "submitType", "search");
                            Vue.set(e2eoListVue.$refs.formComponent, "orderTarget", null);
                            Vue.set(e2eoListVue.$refs.formComponent, "orderType", null);
                            Vue.set(e2eoListVue.$refs.formComponent, "formSchema", this.getRequestData());
						}

					},
					doPagingScale:function(event) {
						this.$set(this.pagingData, "pageIndex", 1);
						this.$set(this.pagingData, "pageRowSize", event.target.value);
						this.$set(this.pagingData, "pageSize", null);
						this.$set(this.pagingData, "pageStartIndex", null);

						Vue.set(e2eoListVue.$refs.formComponent, "submitType", "search");
						Vue.set(e2eoListVue.$refs.formComponent, "formSchema", this.getRequestData());
					},
					goPaging:function(selectedPage) {
						var totalPageSize = Math.ceil(this.totalCount / this.pagingData.pageRowSize);

						if(selectedPage < 1) {
							this.$set(this.pagingData, "pageIndex", 1);
						} else if(selectedPage > totalPageSize) {
							this.$set(this.pagingData, "pageIndex", Number(totalPageSize));
						} else {
							this.$set(this.pagingData, "pageIndex", Number(selectedPage));
						}

						Vue.set(e2eoListVue.$refs.formComponent, "submitType", "search");
						Vue.set(e2eoListVue.$refs.formComponent, "formSchema", this.getRequestData());
					}
				}
			});
		}
	};
})();
