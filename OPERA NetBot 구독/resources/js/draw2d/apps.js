var canvas;
var topology = {
	canvas:{
		snapInterval:20,
		editPolicy:{
			corona:{
				type:"draw2d.policy.canvas.CoronaDecorationPolicy",
				object:new draw2d.policy.canvas.CoronaDecorationPolicy()
			},
			readOnly:{
				type:"draw2d.policy.canvas.ReadOnlySelectionPolicy",
				object:new draw2d.policy.canvas.ReadOnlySelectionPolicy()
			}
		}
	},
	component:{
		image:{
			type:"draw2d.shape.basic.Image",
			size:50,
			resizeable:{
				true:true,
				false:false
			}
		},
		raft:{
			type:"draw2d.shape.composite.Raft",
			size:200,
			bgColor:"#C8C8C8",
			color:"#1B1B1B"
		},
		connection:{
			type:"draw2d.Connection",
			alpha:1,
			angle:0,
			color:"#129CE4",
			outlineColor:"none",
			outlineStroke:0,
			radius:3,
			stroke:2,
			router:{
				direct:new draw2d.layout.connection.DirectRouter(),
				spline:new draw2d.layout.connection.SplineConnectionRouter(),
				manhattan:new draw2d.layout.connection.ManhattanConnectionRouter(),
				circuit:new draw2d.layout.connection.CircuitConnectionRouter()
			}
		},
		label:{
			size:14,
			bold:true,
			type:"draw2d.shape.basic.Label"
		}
	},
	port:{
		hybrid:{
			type:"draw2d.HybridPort",
			name:"hybrid"
		},
		input:{
			type:"draw2d.InputPort",
			name:"input"
		},
		output:{
			type:"draw2d.OutputPort",
			name:"output"
		}
	},
	locator:{
		input:{
			type:"draw2d.layout.locator.InputPortLocator",
			object:new draw2d.layout.locator.InputPortLocator()
		},
		output:{
			type:"draw2d.layout.locator.OutputPortLocator",
			object:new draw2d.layout.locator.OutputPortLocator()
		},
		center:{
			type:"draw2d.layout.locator.CenterLocator",
			object:new draw2d.layout.locator.CenterLocator()
		},
		left:{
			type:"draw2d.layout.locator.LeftLocator",
			object:new draw2d.layout.locator.LeftLocator()
		},
		right:{
			type:"draw2d.layout.locator.RightLocator",
			object:new draw2d.layout.locator.RightLocator()
		},
		top:{
			type:"draw2d.layout.locator.TopLocator",
			object:new draw2d.layout.locator.TopLocator()
		},
		bottom:{
			type:"draw2d.layout.locator.BottomLocator",
			object:new draw2d.layout.locator.BottomLocator()
		},
		leftTop:{
			type:"draw2d.layout.locator.LeftTopLocator",
			object:new draw2d.layout.locator.LeftTopLocator()
		},
		rightTop:{
			type:"draw2d.layout.locator.RightTopLocator",
			object:new draw2d.layout.locator.RightTopLocator()
		},
		leftBottom:{
			type:"draw2d.layout.locator.LeftBottomLocator",
			object:new draw2d.layout.locator.LeftBottomLocator()
		},
		rightBottom:{
			type:"draw2d.layout.locator.RightBottomLocator",
			object:new draw2d.layout.locator.RightBottomLocator()
		}
	},
	editor:{
		label:{
			dialog:new draw2d.ui.LabelEditor(),
			inplace:new draw2d.ui.LabelInplaceEditor()
		}
	}
};

var apps = apps || (function() {
	return {
		initCanvas:function(canvasName, callback) {
			canvas = new draw2d.Canvas(canvasName);
			canvas.setScrollArea(document.getElementById(canvasName));
			canvas.installEditPolicy(topology.canvas.editPolicy.corona.object);

			//캔버스 내 컴퍼넌트 삭제
			canvas.on("figure:remove", function(emitter, event) {
				globalEventBus.$emit(e2eoVueGlobalEvent.draw2d.exportComponents, null);
			}).on("figure:add", function(emitter, event) {
				globalEventBus.$emit(e2eoVueGlobalEvent.draw2d.exportComponents, null);
			});

			if(!e2eo.isEmpty(callback)) {
				callback();
			}
		},
		clearCanvas:function() {
			canvas.clear();
		},
		exportComponents:function() {
			var resultComponents = new Array();

			new draw2d.io.json.Writer().marshal(canvas, function(components) {
				$.each(components, function(i, component) {
					canvas.getFigures().each(function(j, label) {
						if(component.id == label.id) {
							component.label = {
								id:label.children.data[0].figure.id,
								text:label.children.data[0].figure.text
							}
						}
					});
					resultComponents.push(component);
				});
			});

			return resultComponents;
		},
		importComponents:function(components, targetCanvas, isReadOnly, callback) {
			var resultCanvas = canvas;
			if(!e2eo.isEmpty(targetCanvas)) {
				resultCanvas = targetCanvas;
			}

			$.each(JSON.parse(components), $.proxy(function(i, component) {
				this.createComponent(component, resultCanvas);
			}, this));

			if(isReadOnly) {
				resultCanvas.uninstallEditPolicy(topology.canvas.editPolicy.corona.object).installEditPolicy(topology.canvas.editPolicy.readOnly.object);
			} else {
				resultCanvas.uninstallEditPolicy(topology.canvas.editPolicy.readOnly.object).installEditPolicy(topology.canvas.editPolicy.corona.object);
			}

			if (callback != null) {
				callback();
			}
		},
		createComponent:function(component, targetCanvas) {
			if(component.type == topology.component.raft.type) {
				var resultComponent = new draw2d.shape.composite.Raft();
				resultComponent.setId(component.id);
				resultComponent.setWidth(component.width);
				resultComponent.setHeight(component.height);
				resultComponent.setBackgroundColor(component.bgColor);
				resultComponent.setColor(component.color);
				resultComponent.setUserData(component.userData);
				resultComponent.setAlpha(0.2);
				this.createLabel(component.label, function(label) {
					resultComponent.add(label, topology.locator.bottom.object, 0);
				});
				$.each(component.ports, $.proxy(function(j, port) {
					this.createPort(port, function(portObject) {
						resultComponent.createPort(topology.port.hybrid.name, portObject).setId(port.id).setName(port.name);
					});
				}, this));

				targetCanvas.add(resultComponent, component.x, component.y);
			} else if(component.type == topology.component.image.type) {
				var resultComponent = new draw2d.shape.basic.Image();
				resultComponent.setPath(component.path);
				resultComponent.setId(component.id);
				resultComponent.setResizeable(topology.component.image.resizeable.false);
				resultComponent.setWidth(topology.component.image.size);
				resultComponent.setHeight(topology.component.image.size);
				resultComponent.setUserData(component.userData);
				this.createLabel(component.label, function(label) {
					resultComponent.add(label, topology.locator.bottom.object, 0);
				});
				$.each(component.ports, $.proxy(function(j, port) {
					this.createPort(port, function(portObject) {
						resultComponent.createPort(topology.port.hybrid.name, portObject).setId(port.id).setName(port.name);
					});
				}, this));

				targetCanvas.add(resultComponent, component.x, component.y);
			} else if(component.type == topology.component.connection.type) {
				var resultConnection = new draw2d.Connection();
				resultConnection.setId(component.id);
				resultConnection.setRouter(topology.component.connection.router.direct);
				resultConnection.setAlpha(topology.component.connection.alpha);
				resultConnection.setRotationAngle(topology.component.connection.angle);
				/*resultConnection.setColor(topology.component.connection.color);
				resultConnection.setOutlineColor(topology.component.connection.outlineColor);
				resultConnection.setOutlineStroke(topology.component.connection.outlineStroke);
				resultConnection.setRadius(topology.component.connection.radius);
				resultConnection.setStroke(topology.component.connection.stroke);*/
				$.each(component.vertex, function(i, vertex) {
					resultConnection.setVertex(i, vertex.x, vertex.y);
				});

				targetCanvas.getAllPorts().each(function(j, port) {
					if(component.source.node == port.parent.id) {
						if(component.source.port == port.name) {
							resultConnection.setSource(port);
						}
					} else if(component.target.node == port.parent.id) {
						if(component.target.port == port.name) {
							resultConnection.setTarget(port);
						}
					}
				});
				targetCanvas.add(resultConnection);
			}
		},
		createPort:function(port, callback) {
			var portObject = topology.locator.center.object;
			if(port.locator == topology.locator.input.type) {
				portObject = topology.locator.input.object;
			} else if(port.locator == topology.locator.output.type) {
				portObject = topology.locator.output.object;
			} else if(port.locator == topology.locator.center.type) {
				portObject = topology.locator.center.object;
			} else if(port.locator == topology.locator.left.type) {
				portObject = topology.locator.left.object;
			} else if(port.locator == topology.locator.right.type) {
				portObject = topology.locator.right.object;
			} else if(port.locator == topology.locator.top.type) {
				portObject = topology.locator.top.object;
			} else if(port.locator == topology.locator.bottom.type) {
				portObject = topology.locator.bottom.object;
			} else if(port.locator == topology.locator.leftTop.type) {
				portObject = topology.locator.leftTop.object;
			} else if(port.locator == topology.locator.rightTop.type) {
				portObject = topology.locator.rightTop.object;
			} else if(port.locator == topology.locator.leftBottom.type) {
				portObject = topology.locator.leftBottom.object;
			} else if(port.locator == topology.locator.rightBottom.type) {
				portObject = topology.locator.rightBottom.object;
			}

			callback(portObject);
		},
		createLabel:function(componentLabel, callback) {
			var label = new draw2d.shape.basic.Label();
			label.setId(componentLabel.id);
			label.setText(componentLabel.text);
			label.setColor(null);
			label.setFontSize(topology.component.label.size);
			label.setBold(topology.component.label.bold);
			label.installEditor(topology.editor.label.inplace);

			//label 텍스트 변경
			label.on("change:text", function(emitter, event) {
				globalEventBus.$emit(e2eoVueGlobalEvent.draw2d.exportComponents, null);
			});

			callback(label);
		},
		onDrop:function(droppedDomNode, x, y, shiftKey, ctrlKey) {
			var isReadOnly = false;
			canvas.editPolicy.each(function(i, policy) {
				if(topology.canvas.editPolicy.readOnly.type == policy.NAME) {
					isReadOnly = true;
				}
			});

			if(!isReadOnly) {
				/*var componentPosition = canvas.fromDocumentToCanvasCoordinate(x - Math.floor(droppedDomNode[0].clientWidth/2), y - Math.floor(droppedDomNode[0].clientHeight/2));
				console.log("Component Position [X/Y] : ", componentPosition.x, " / ", componentPosition.y);*/

				var componentPosition = {
					x:x - Math.floor(droppedDomNode[0].clientWidth/2),
					y:y - Math.floor(droppedDomNode[0].clientHeight/2)
				};
				//console.log("Component Position [X/Y] : ", componentPosition);

				var componentPath = droppedDomNode[0].currentSrc;
				var componentUserData = droppedDomNode[0].dataset;
				var componentLabelText = componentUserData.name;

				var locatorArray = [
					topology.locator.top.type,
					topology.locator.bottom.type,
					topology.locator.left.type,
					topology.locator.right.type,
					topology.locator.leftTop.type,
					topology.locator.rightTop.type,
					topology.locator.rightBottom.type,
					topology.locator.leftBottom.type
				];

				var ports = new Array();
				if(!e2eo.isEmpty(componentUserData.nsd_id)) {
					//Nested Component
					$.each(JSON.parse(componentUserData.sapd_id), function(i, sap) {
						ports.push({
							id:sap.id,
							locator:locatorArray[i],
							name:sap.cpd.profileId.concat(".", sap.id)
						});
					});
				} else {
					if(e2eo.isEmpty(componentUserData.nsd_id) && Number(componentUserData.type) < 4) {
						//VNF Component
						for(var i = 0; i < locatorArray.length; i++) {
							var portId = e2eo.getUUID();
							ports.push({
								id:portId,
								locator:locatorArray[i],
								name:componentLabelText.concat(".", portId)
							});
						}
					} else if(e2eo.isEmpty(componentUserData.nsd_id) && (Number(componentUserData.type) == 4 || Number(componentUserData.type) == 5)) {
						//Connectivity
						var portId = e2eo.getUUID();
						for(var i = 0; i < locatorArray.length; i++) {
							ports.push({
								id:portId,
								locator:locatorArray[i],
								name:componentLabelText.concat("_", String(i), ".", portId)
							});
						}
					} else {
						var portId = e2eo.getUUID();
						ports.push({
							id:portId,
							locator:topology.locator.center.type,
							name:componentLabelText.concat(".", portId)
						});
					}
				}

				var component = new Object();
				component.id = e2eo.getUUID();
				component.x = componentPosition.x;
				component.y = componentPosition.y;
				component.userData = componentUserData;
				component.label = {
					id:e2eo.getUUID(),
					text:componentLabelText
				};
				//TODO : VNF타입을 확인할 수 있는 조건으로 변경 필요
				if(componentPath.indexOf("GROUP") >= 0 || Number(componentUserData.type) == 6 || Number(componentUserData.type) == 7) {
					component.type = topology.component.raft.type;
					component.bgColor = topology.component.raft.bgColor;
					component.color = topology.component.raft.color;
					component.width = topology.component.raft.size;
					component.height = topology.component.raft.size;
					component.ports = new Array();
				} else {
					component.type = topology.component.image.type;
					component.path = componentPath;
					component.ports = ports;
				}

				this.createComponent(component, canvas);
			}
		},
		onDoubleClick:function(component) {
			//TODO : 더블클릭 이벤트 추가
			//TODO : read only canvas일 경우, 더블클릭 이벤트 바인딩 안됨
			if(component.NAME == topology.component.raft.type) {

			} else if(component.NAME == topology.component.image.type) {
				globalEventBus.$emit(e2eoVueGlobalEvent.draw2d.nestedComponentInfo, component.userData.nsd_id);
			}
		},
		setComponentToolTip:function(component, isShow) {
			/*if(!e2eo.isEmpty(canvas) && component.NAME != topology.component.label.type && component.NAME != topology.component.connection.type) {
				//var componentPosition = canvas.fromCanvasToDocumentCoordinate(component.x, component.y);
				var componentPosition = canvas.fromCanvasToDocumentCoordinate(0, 0);

				var toolTipText = new Array();
				toolTipText.push(e2eo.isEmpty(component.userData.site) ? "" : component.userData.site);
				toolTipText.push(e2eo.isEmpty(component.userData.name) ? "" : component.userData.name);
				toolTipText.push(e2eo.isEmpty(component.userData.provider) ? "" : component.userData.provider);
				toolTipText.push(e2eo.isEmpty(component.userData.version) ? "" : component.userData.version);

				if(isShow) {
					$("body").append(
							$(document.createElement("div")).attr("id", "component-tooltip-".concat(component.id)).css({
								position:"absolute",
								border:"1px solid #000000",
								backgroundColor:"#ffffff",
								padding:"5px",
								top:componentPosition.y,
								left:componentPosition.x
							}).append(toolTipText.join(" | "))
					);
				} else {
					$("#".concat("component-tooltip-".concat(component.id))).remove();
				}
			}*/

			if(!e2eo.isEmpty(canvas) && component.NAME != topology.component.label.type && component.NAME != topology.component.connection.type) {
				globalEventBus.$emit(e2eoVueGlobalEvent.draw2d.vnfComponentInfo, $.extend(true, new Object(), component, {
					isShow:isShow
				}));
			}
		},
		setPortToolTip:function(port, isShow) {
			//var portPosition = canvas.fromCanvasToDocumentCoordinate(port.shape.attrs.cx, port.shape.attrs.cy);
			/*var portPosition = canvas.fromCanvasToDocumentCoordinate(0, 50);

			if(isShow) {
				$("body").append(
						$(document.createElement("div")).attr("id", "port-tooltip-".concat(port.id)).css({
							position:"absolute",
							border:"1px solid #000000",
							backgroundColor:"#ffffff",
							padding:"5px",
							top:portPosition.y,
							left:portPosition.x
						}).append(port.name)
				);
			} else {
				$("#".concat("port-tooltip-".concat(port.id))).remove();
			}*/

			globalEventBus.$emit(e2eoVueGlobalEvent.draw2d.nestedSapInfo, $.extend(true, new Object(), port, {
				isShow:isShow
			}));
		},
		setConnectionRouter:function() {
			return topology.component.connection.router.direct;
		},
		onConnected:function() {
			globalEventBus.$emit(e2eoVueGlobalEvent.draw2d.exportComponents, null);
		},
		setComponentObj: function(args) {
			var name = args.name,
				type = args.type,
				ports = args.ports,
				position = args.position ? args.position : {},
				path = args.path,
				userData = args.userData,
				userDataKeyObj = function () {
					return {
						cold_time: null,
						collection_period: null,
						id: null,
						json: null,
						max_threshold: null,
						min_threshold: null,
						name: null,
						nfvo_id: null,
						nfvo_name: null,
						nsd_id: null,
						pkg_id: null,
                        vnf_product_name:null,
						pm_job_id: null,
						provider: null,
						reporting_period: null,
						sapd_id: null,
						site: null,
						topology: null,
						type: null,
						version: null,
						vnf_ins_id: null,
                        vnf_instance_data: null,
						xml: null,
                        marketplace_id:null
					};
				},
				result = {
					id: e2eo.getUUID(),
					label: {
						id: e2eo.getUUID(),
						text: name
					},
					type: type,
					userData: (function(f) {
						var obj = userDataKeyObj();

						Object.keys(obj).forEach(function(key) {
							obj[key] = f[key] ? f[key] : '';

							if (!obj[key]) {
                                var underscored = key.replace(/_([a-z])/g, function (g) { return g[1].toUpperCase(); });

                                obj[key] = f[underscored] ? f[underscored] : '';
                            }
						});

						return obj;
					})(userData),
					x: position.x ? position.x : 0,
					y: position.y ? position.y : 0
				};

			if (ports && ports.length > 0) {
				var vlPortId = e2eo.getUUID();
				result.ports = (function(ps) {
					var ret = [];
					ps.forEach(function(p, i) {
						var portId = args.portName.indexOf('VL') > -1 || args.portName.indexOf('SAP') > -1 ? vlPortId : e2eo.getUUID();
						var portName = args.portName.indexOf('VL') > -1 || args.portName.indexOf('SAP') > -1 ?  args.portName.trim().replace(/\s/g, "_").concat('_', i, ".", portId) : args.portName.trim().replace(/\s/g, "_").concat(".", portId);
                        //var portId = result.label.text.indexOf('VL') > -1 || result.label.text.indexOf('SAP') > -1 ? vlPortId : e2eo.getUUID();

						ret.push({
							id: portId,
							name: portName,
							locator: topology.locator[p].type
						});
					});

					return ret;
				})(ports);

			}

			if (path) {
				result.path = path;
			}

			return result;
		},
		setConnectionObj: function(src, srcDirection, target, targetDirection) {
			return {
				id: e2eo.getUUID(),
				type: topology.component.connection.type,
				source: {
					node: src.id,
					port: (function(ports, dir) {
						var ret = null;

						$.each(ports, function(i, port) {
							if (port.locator === dir) {
								ret = port.name;
							}
						});

						return ret;
					})(src.ports, srcDirection)
				},
				target: {
					node: target.id,
					port: (function(ports, dir) {
						var ret = null;

						if (ports == null) {
							return target.port.name;
						}

						$.each(ports, function(i, port) {
							if (port.locator === dir) {
								ret = port.name;
							}
						});

						return ret;
					})(target.ports, targetDirection)
				},
				userData: {}
			};
		},
		setEdgeNsd: function(upf) {
			var connectionInfoData = {},
				dfd = $.Deferred();

			e2eo.callAjax({
					url: '/sr/design/getConnectionInfoListAjax',
					type: 'get',
					data: {"connType" : "edge"}
				}, function(res) {

				$.each(res.data, function (i, conn) {
					connectionInfoData[conn.name.toUpperCase()] = conn;
				});

				var upfComp = apps.setComponentObj({
						name: upf.upfProfileId,
                        portName: upf.name,
						ports: ['left', 'right', 'top', 'bottom', 'leftTop', 'rightTop', 'leftBottom', 'rightBottom'],
						type: topology.component.image.type,
						path: '/resources/images/contents/UPF.svg',
						userData: upf
					}),
					sapComp = apps.setComponentObj({
						name: 'SAP_0',
                        portName: 'SAP',
						ports: ['left', 'right', 'top', 'bottom', 'leftTop', 'rightTop', 'leftBottom', 'rightBottom'],
						type: topology.component.image.type,
						path: '/resources/images/contents/SAP.svg',
						userData: connectionInfoData.SAP
					}),
					vlComp = apps.setComponentObj({
						name: 'VL_0',
                        portName: 'VL',
						ports: ['left', 'right', 'top', 'bottom', 'leftTop', 'rightTop', 'leftBottom', 'rightBottom'],
						type: topology.component.image.type,
						path: '/resources/images/contents/VL.svg',
						userData: connectionInfoData.VL
					}),
					size = {
						width: canvas.initialWidth / 2,
						height: canvas.initialHeight / 2,
						image: topology.component.image.size
					},
					offsetX,
					offsetY,
					offsetMargin = 100;

				// Set UPF location
				upfComp.x = offsetX = (size.width / 2) - (size.image / 2);
				upfComp.y = offsetY = (size.height / 2) - (size.image / 2);

				// Set virtual link location
				vlComp.x = offsetX;
				vlComp.y = offsetY - offsetMargin;

				// Set SAP location
				sapComp.x = offsetX;
				sapComp.y = vlComp.y - offsetMargin;

				dfd.resolve([
					upfComp,
					vlComp,
					sapComp,
					apps.setConnectionObj(upfComp, topology.locator.top.type, vlComp, topology.locator.bottom.type),
					apps.setConnectionObj(vlComp, topology.locator.top.type, sapComp, topology.locator.bottom.type)
				]);
			});

			return dfd.promise();
		},
        /**
         * @description
         *   SR > design > On Boarding (topology data)
         * @param {*} components
         * @return $promise
         * */
        setComponentsToJsonData: function(components) {
		    var deferred = $.Deferred(),
                connectionInfoData = {};

            e2eo.callAjax({
                url: '/sr/design/getConnectionInfoListAjax',
                type: 'get',
                data: {"connType" : "total"}
            }, function(res) {
				if (res.resultCode !== 200) {
					throw TypeError('Illegal Request Error');
				}

				// Connector list to object
				$.each(res.data, function (i, conn) {
					connectionInfoData[conn.name.toUpperCase()] = conn;
				});

				connectionInfoData.VL.version = "new";

				// NSR
				var commonCore = components.nsr,
					edge = components.nsd,
					coreTopology = JSON.parse(commonCore.topogyInfo),
					edgeTopology = JSON.parse(edge.topogyInfo),
					targetInfo = (function () {
						var ret = {};

						components.smf.forEach(function (s) {
							var nsdId = s.smfNsdId,
								smfId = s.smfId;

							if (ret[nsdId] == null) {
								ret[nsdId] = new Set();
							}

							ret[nsdId].add(smfId);
						});

						return ret;
					})(),
					coreConnections = coreTopology.filter(function (component) {
						return component.type === topology.component.connection.type;
					}),
					targetNsdIds = Object.keys(targetInfo),
					targetNsds = [];

				// Find Target NSD
				targetNsdIds.forEach(function(nsdId) {
					for (var idx = 0; idx < coreTopology.length; idx++) {
						var nsd = coreTopology[idx],
							nsdUserData = nsd.userData;

						if (nsdUserData && nsdUserData.nsd_id === nsdId) {
							targetNsds.push(nsd);
							break;
						}
					}
				});

				var edgeSapList = [];

				edgeTopology.forEach(function(component) {
					if (/SAP/gi.test(component.userData.name)) {
						edgeSapList.push(component);
					}
				});

				var nsdTopology = apps.setComponentObj({
					name: edge.nsdName,
					type: topology.component.image.type,
					ports: [],
					path: '/resources/images/contents/NESTED.svg',
					userData: Object.assign(edge, {
						id:e2eo.getUUID(),
						topology: edge.topogyInfo,
						nfvo_id: edge.targetNfvos[0].nfvoId,
						site: edge.targetNfvos[0].siteName,
						nfvo_name: edge.targetNfvos[0].nfvoName
					})
				});

				nsdTopology.userData.type = "1";
				nsdTopology.userData.name = nsdTopology.label.text;
				nsdTopology.userData.xml = [];
				nsdTopology.ports = [
					{
						id: edgeSapList[0].id,
						name: edgeSapList[0].label.text + '.' + edgeSapList[0].id,
						locator: topology.locator.top.type
					}
				];

				var marginX = 30,
					size = {
						width: canvas.initialWidth,
						height: canvas.initialHeight
					},
					imageSize = topology.component.image.size,
					offsetX = (size.width / 2) - (((imageSize + marginX) - marginX) / 2),
					offsetY = (size.height / 2)
				;

				nsdTopology.x = (size.width / 2) - (imageSize / 2);
				nsdTopology.y = offsetY + 100;

				var vl = apps.setComponentObj({
						name: 'VL_EDGE_1', // sr onboard 에서 저장 에러 안나게 임시
						portName: 'VL_EDGE',
						ports: ['left', 'right', 'top', 'bottom', 'leftTop', 'rightTop', 'leftBottom', 'rightBottom'],
						type: topology.component.image.type,
						path: '/resources/images/contents/VL.svg',
						userData: connectionInfoData.VL
					}),
					c1 = apps.setConnectionObj(nsdTopology, topology.locator.top.type, vl, topology.locator.bottom.type);

				vl.x = offsetX;
				vl.y = offsetY;

				// Set NSD TOPOLOGY END
				var vlTopologies = [vl, c1];

				// Connect Edge and Common core
				targetNsds.forEach(function(targetNsdComponent) {
					var nsdId = targetNsdComponent.userData.nsd_id,
						nsdTopology = JSON.parse(targetNsdComponent.userData.topology),
						componentHash = (function() {
							var ret = {
								connections: [],
								vnfHash: {},
								vlHash: {},
								sapHash: {}
							};

							nsdTopology.forEach(function(comp) {
								var id = comp.id;

								if (comp.type === 'draw2d.Connection') {
									ret.connections.push(comp);
								} else {
									var typeNum = parseInt(comp.userData.type);

									switch (typeNum) {
										case 1:
											ret.vnfHash[id] = comp;
											break;
										case 4:
											ret.vlHash[id] = comp;
											break;
										case 5:
											ret.sapHash[id] = comp;
									}
								}
							});

							return ret;
						})(),
						vnfHash = componentHash.vnfHash,
						vlHash = componentHash.vlHash,
						sapHash = componentHash.sapHash,
						connections = componentHash.connections,
						targetSmfs = Array.from(targetInfo[nsdId]);

					// Check valid SAP
					Object.keys(sapHash).forEach(function(sapId) {
						for (var i = 0; i < connections.length; i++) {
							var connection = connections[i],
								src = connection.source,
								dst = connection.target,
								srcId = src.node,
								dstId = dst.node,
								trgtId = null;

							if (srcId === sapId) {
								trgtId = dstId;
							} else if (dstId === sapId) {
								trgtId = srcId;
							}

							if (trgtId != null && vlHash[trgtId] != null) {
								for (var j = 0; j < connections.length; j++) {
									var conn = connections[j],
										_src = conn.source,
										_dst = conn.target,
										_trgtId = null;

									if (_src.node === trgtId) {
										_trgtId = _dst.node;
									} else if (_dst.node === trgtId) {
										_trgtId = _src.node;
									}

									if (_trgtId != null && vnfHash[_trgtId] != null && /AMF/gi.test(vnfHash[_trgtId].userData.name)) {
										// Remove invalid sap
										delete sapHash[sapId];
									}

									coreConnections.forEach(function(conn) {
										var srcPort = conn.source.port,
											dstPort = conn.target.port;

										Object.keys(sapHash).forEach(function(k) {
											if (srcPort.indexOf(k) !== -1 || dstPort.indexOf(k) !== -1) {
												delete sapHash[k];
											}
										});
									});
								}
							}
						}
					});

					var targetSAPId = Object.keys(sapHash)[0];

					var conn = apps.setConnectionObj(vl, topology.locator.top.type, targetNsdComponent, (function () {
						for (var j = 0; j < targetNsdComponent.ports.length; j++) {
							var port = targetNsdComponent.ports[j];

							if (port.id === targetSAPId) {
								return port.locator;
							}
						}
					})());

					vlTopologies.push(conn);
				});

				var total = coreTopology.concat(nsdTopology).concat(vlTopologies);

				deferred.resolve({
					total: total,
					nsd: nsdTopology,
					nsr: coreTopology
				});
			});

            return deferred.promise();
        }
	};
})();