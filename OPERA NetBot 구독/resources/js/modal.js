function openModal(selector, option) {
	if (option == null) {
		option = new Object();
	}
	$(selector).modal(option);
	$("#simplemodal-container").draggable({
		handle: "h2"
	});
}

function closeModal() {
    $.modal.close();
}

//openAlertModal("title", "body", "callback", "button text");
function openAlertModal(sMessage, sDescription, fnOK, sOK) {
    try {
        if (sMessage == null) {
            sMessage = "";
        }
        if (sDescription == null) {
            sDescription = "";
        }
        if (sOK == null) {
            sOK = "OK";
        }
        $("#alertModalDiv div.pop_tit").text(sMessage);
        $("#alertModalDiv p.txt").text(sDescription);
        $("#alertModalDiv a#alertModal_okButton").text(sOK);

        $("#alertModalDiv").draggable();
        $("#alertModal_okButton").off("click");
        $("#alertModal_okButton").on("click", function(){
            closeModal();
            $("#alertModalDiv").hide();
            if (fnOK != null) {
                fnOK.call();
            }
        })
        openModal("#alertModalDiv");
    } catch (e) {
        console.log("### [ openAlertModal ] Catch ");
        console.log(e);
    }

}

/*function openAlertModal2(title, msg, okfn, okmsg) {
}
function confirmModal2(title, msg, okfn, cancelfn, okmsg, cancelmsg, isModal) {
	if(title == null) title = '';
	if(msg == null) msg = '';
	if(okmsg == null) okmsg = 'OK';
	if(cancelmsg == null) cancelmsg = "Cancel";
	if(isModal == null) isModal = true;

	var key = ".commonModal";
	$(key + " .txt").text(msg);
	$(key + " .ok").on('click',function(){
		if(okfn != null) okfn.call();
	});
	$(key + " .cancel").on('click',function(){
		if(cancelfn != null) cancelfn.call();
	});
	pop_open(key);
}*/

function confirmModal(sMessage, sDescription, fnOK, fnCancel, sOK, sCancel, bModal) {
	if (sMessage == null) {
		sMessage = "";
	}
	if (sDescription == null) {
		sDescription = "";
	}
	if (sOK == null) {
		sOK = "OK";
	}
	if (sCancel == null) {
		sCancel = "Cancel";
	}
	if (bModal == null) {
		bModal = true;
	}

	var sHtml = "";
	if( !$(".popup_mask") ){
		sHtml += "<div class=\"popup_mask\"></div>";
	}

	sHtml += "<div id=\"confirmModalDiv\" class=\"popup pop_alarm\">";
	sHtml += "<div class=\"pop_tit\">"+sMessage+"</div>";
	sHtml += "	<div class=\"pop_con_wrap\">";
	sHtml += "		<div class=\"confirm_dialog\">";
	sHtml += "			<p class=\"txt\">"+sDescription+"</p>";
	sHtml += "		</div>";
	sHtml += "		<div class=\"btn_area\">";
	sHtml += "			<a href=\"javascript:void(0);\" id=\"confirmModal_okButton\" class=\"btn_l btn_co01\">"+sOK+"</a>";
	sHtml += "			<a href=\"javascript:void(0);\" id=\"confirmModal_cancelButton\" class=\"btn_l btn_co01\">"+sCancel+"</a>";
	sHtml += "		</div>";
	sHtml += "</div>";
	sHtml += "</div>";
	$("body").append(sHtml);
	$("#confirmModalDiv").draggable();
	if (bModal) {
		$("#confirmModal_okButton").click(function() {
			closeModal();
			$("#confirmModalDiv").remove();
			if (fnOK != null) {
				fnOK.call();
			}
		});
		$("#confirmModal_cancelButton").click(function() {
			closeModal();
			$("#confirmModalDiv").remove();
			if (fnCancel != null) {
				fnCancel.call();
			}
		});

		openModal("#confirmModalDiv");
	} else {
		$("#confirmModal_okButton").click(function() {
			$("#confirmModalDiv").hide();
			if (fnOK != null) {
				fnOK.call();
			}
		});
		$("#confirmModal_cancelButton").click(function() {
			$("#confirmModalDiv").hide();
			if (fnCancel != null) {
				fnCancel.call();
			}
		});

		var left = ($(window).width() - $("#confirmModalDiv").width()) / 2;
		var top = ($(window).height() - $("#confirmModalDiv").height()) / 2;
		$("#confirmModalDiv").css("z-index", 1003);
		$("#confirmModalDiv").css("left", left+"px");
		$("#confirmModalDiv").css("top", top+"px");
		$("#confirmModalDiv").css("position", "absolute");
		$("#confirmModalDiv").show();
	}
}