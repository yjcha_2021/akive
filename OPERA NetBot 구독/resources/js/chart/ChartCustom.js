//SeveryChart 초기화
Chart.pluginService.register({
	beforeDraw: function (chart) {
		if (chart.config.options.elements.center) {
			//Get ctx from string
			var ctx = chart.chart.ctx;
			var centerConfig = chart.config.options.elements.center;
			var color = centerConfig.color || '#000';
			var txt = centerConfig.text;
			var txtToStr = txt.toString();
			ctx.fillStyle = color;

			var width = chart.chart.width;
            var height = chart.chart.height;

            var fontSize = (height / 114).toFixed(2);
            chart.chart.ctx.font = fontSize + "em Verdana";
            chart.chart.ctx.textBaseline = "middle";

            var text = "Alarms";
			var textX = Math.round((width - chart.chart.ctx.measureText(text).width) / 2);
			var textY = height / 4 * 1.2;
            ctx.fillText(text, textX, textY);

            /* 자리수에 따른 폰트 사이즈조절*/
			if(txtToStr.length==1){
				fontSize = 3;
			} else if(txtToStr.length==2) {
                fontSize = 2.6;
            } else if(txtToStr.length==3) {
                fontSize = 2.2;
            } else if(txtToStr.length==4) {
                fontSize = 1.8;
            } else if(txtToStr.length==5) {
                fontSize = 1.5;
            } else if(txtToStr.length==6) {
                fontSize = 1.2;
            }

            /* Summary 값 셋팅 */
            chart.chart.ctx.font = fontSize + "em Verdana";
            chart.chart.ctx.textBaseline = "middle";
            var text2 =  txt;
            var textX2 = Math.round((width - chart.chart.ctx.measureText(text2).width) / 2);
            var textY2 = height / 4 *2.5;
            ctx.fillText(text2, textX2, textY2);
		}
	}
});
