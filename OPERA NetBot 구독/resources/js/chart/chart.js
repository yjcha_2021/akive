var chart = chart || (function() {
			return {
				countChart: function (options) {
					return {
						name: 'countchart-component',
						props: {
							countChartData: Object
						},
						data: function () {
							return {
								chartData: Object
							}
						},
						created: function() {
							this.$watch("countChartData", function(newValue, orgValue) {
								this.$set(this, "chartData", newValue);
							}, {
								deep:true
							});
						},
						components: {
							"doughnut-chart": {
								extends: VueChartJs.Doughnut,
								mixins: [VueChartJs.mixins.reactiveProp],
								data: function () {
									return {
										options: (options !== undefined && options !== null) ? options : {
											responsive: false,
											maintainAspectRatio: false,
											legend : {
												display : false
											},
											tooltips: {
												enabled: true,
												mode: 'single',
												callbacks: {
													label: function(tooltipItems, data) {
														var total = 0;
														var currentValue = data.datasets[0].data[tooltipItems.index];
														for (var j = 0; j < data.datasets[0].data.length; j++) {
															total += data.datasets[0].data[j];
														}
														var percentage = Math.floor(((currentValue/total) * 100)+0.5);
														return percentage + "%";
													}
												}
											}
										}
									}
								}
							}
						},
						template: `<doughnut-chart :chart-data="chartData.dataCollection" :width="100" :height="100" ref="doughnutChart"></doughnut-chart>`
					}
				},
				severityChart: function (options) {
					return {
						name: 'severitychart-component',
						props: {
							severityChartData: Object
						},
						data: function () {
							return {
								chartData: Object
							}
						},
						created: function() {
							this.$watch("severityChartData", function(newValue, orgValue) {
								this.$set(this, "chartData", newValue);
							}, {
								deep:true
							});
						},
						components: {
							"doughnut-chart": {
								extends: VueChartJs.Doughnut,
								mixins: [VueChartJs.mixins.reactiveProp],
								props: {
									chartData: Object
								},
								watch: {
									chartData: function () {
										var total = 0;

										if (this.$data._chart.data.datasets.length > 0){
											for (var i = 0; i < this.$data._chart.data.datasets[0].data.length; i++) {
												total = total + this.$data._chart.data.datasets[0].data[i];
											}
										}

										this.setSevertyTotalCount(total);
									}
								},
								data: function () {
									return {
										options: (options !== undefined && options !== null) ? options : {
											responsive: false,
											maintainAspectRatio: false,
											elements: {
												center: {
													text:0,
													color: '#444', // Default is #000000
													fontStyle: 'Arial', // Default is Arial
													sidePadding: 20 // Defualt is 20 (as a percentage)
												}
											},
											legend : {
												display : false
											},
											tooltips: {
												enabled: true,
												mode: 'single'
											},
											cutoutPercentage : 85 // 도넛 차트 두께 설정
										}
									}
								},
								methods:{
									setSevertyTotalCount: function (total) {
										this.$data._chart.options.elements.center.text = total;
										this.$data._chart.update();
									}
								}
							}
						},
						template: `<doughnut-chart :chart-data="chartData.dataCollection" :width="100" :height="100" ref="severityChart"></doughnut-chart>`
					}
				},
				barChart: function (options) {
					return {
						name: 'barchart-component',
						props: {
							barChartData: Object,
                            width: Number,
                            height: Number
						},
						data: function () {
							return {
								chartData: Object
							}
						},
						created: function() {
							this.$watch("barChartData", function(newValue, orgValue) {
								this.$set(this, "chartData", newValue);
							}, {
								deep:true
							});
						},
						components: {
							"bar-chart": {
								extends: VueChartJs.Bar,
								mixins: [VueChartJs.mixins.reactiveProp],
								props: {
									chartData: Object
								},
								data: function () {
									return {
										options: (options !== undefined && options !== null) ? options : {
											responsive: false,
											maintainAspectRatio: false,
											layout: {
												padding: {
													top: 20,
													left: 20
												}
											},
											scales: {
												yAxes: [{
													ticks: {
														suggestedMax: 10,
														maxTicksLimit: 5,
														beginAtZero: true
													},
													gridLines: {
														display: true
													}
													, stacked: true
												}],
												xAxes: [{
													ticks: {
														autoSkip: false,
														// maxRotation: 90,
														// minRotation: 90,
														fontSize: 10
													},
													gridLines: {
														display: false
													}
													, stacked: true
												}]
											},
											legend: {
												display: false
											},
											tooltips: {
												mode: 'label',
												intersect: false,
												callbacks: {
													label: function (tooltipItem, data) {
														return data.datasets[tooltipItem.datasetIndex].label + ": " + tooltipItem.yLabel;
													}
												}
											}
										}
									}
								}
							}
						},
						template: '<bar-chart :chart-data="chartData.dataCollection" :width="width" :height="height" ref="barChart"></bar-chart>'
					}
				},
				lineChart: function (options) {
					return {
						name: 'linechart-component',
						props: {
							chartData: Object
						},
						data: function () {
							return {
								lineData: Object
							}
						},
						created: function() {
							this.$watch("chartData", function(newValue, orgValue) {
								this.$set(this, "lineData", newValue);
							}, {
								deep:true
							});
						},
						components: {
							"line-chart": {
								extends: VueChartJs.Line,
								mixins: [VueChartJs.mixins.reactiveProp],
								props: {
									chartData: Object
								},
								data: function () {
									return {
										options: (options !== undefined && options !== null) ? options : {
											responsive: false,
											maintainAspectRatio: false,
											layout: {
												padding: {
													top: 20,
													left: 20
												}
											},
											scales: {
												yAxes: [{
													ticks: {
														maxTicksLimit: 5,
														beginAtZero: true,
														stepSize: 1
													},
												}],
												xAxes: [{
													ticks: {
														autoSkip: false,
														fontSize: 10
													},
													gridLines: {
														display: false
													}
												}]
											},
											legend: {
												display: false
											},
											tooltips: {
												mode: 'label',
												intersect: false
											}
										}
									}
								}
							}
						},
						template: `<line-chart :chart-data="lineData.dataCollection" :width="800" :height="200" ref="lineChart"></line-chart>`
					}
				},
				gaugeChart: function () {
					return {
						name: 'gaugechart-component',
						props: {
							gaugeData: Object,
							// el: null,
							width: null,
							height: null,
						},
						data: function () {
							return {
								chartData: Object,
								chart: null
							}
						},
						created: function() {
							this.$watch("gaugeData", function(newValue, orgValue) {
								this.$set(this, "chartData", newValue);
								this.drawChart()
							}, {
								deep:true
							});
						},
						methods: {
							drawChart: function () {
								if (this.chart == null) {
									var gChartData = {
										type: "tsgauge",
										data: {
											datasets: [{
												backgroundColor: ["#ff8ab3", "#cedbe2"],
												borderWidth: 0,
												gaugeData: {
													value: this.chartData.gaugeDataDouble,
													valueColor: "#ff8ab3",
													dashboardFlag:true,
													gaugeLabel : this.chartData.gaugeLabel
												},
												gaugeLimits: [0, this.chartData.gaugeDataDouble, 100]
											}]
										},
										options: {
											responsive: false,
											maintainAspectRatio: false,
											events: [],
											arrowColor: "#ff8ab3",
											showMarkers: false,
											indicatorColor:"#444" //화살표 색상
										}
									};
									this.$el.width = this.width;
									this.$el.height = this.height;
									this.chart = new Chart(this.$el.getContext("2d"), gChartData)
								} else {
									this.chart.data.datasets[0].data = [this.chartData.gaugeDataDouble, (100 - this.chartData.gaugeDataDouble)];
									this.chart.data.datasets[0].gaugeData.value = this.chartData.gaugeDataDouble;
									this.chart.data.datasets[0].gaugeLimits = [0, this.chartData.gaugeDataDouble, 100];
									this.chart.update();
								}
							}
						},
						template:   `<canvas style="display: block;"></canvas>`
					}
				},
				horizontalBarChart: function (options) {
					return {
						name: 'horizontal-barchart-component',
						props: {
							barChartData: Object,
							width: Number,
							height: Number
						},
						data: function () {
							return {
								chartData: Object
							}
						},
						created: function() {
							this.$watch("barChartData", function(newValue, orgValue) {
								this.$set(this, "chartData", newValue);
							}, {
								deep:true
							});
						},
						components: {
							"bar-chart": {
								extends: VueChartJs.HorizontalBar,
								mixins: [VueChartJs.mixins.reactiveProp],
								props: { },
								data: function () {
									return {
										options: (options !== undefined && options !== null) ? options : {
											responsive: false,
											maintainAspectRatio: false,
											scales: {
												yAxes: [{
													ticks: {
														maxTicksLimit: 5,
														beginAtZero: true
													},
													gridLines: {
														display: true
													},
													stacked: true
												}],
												xAxes: [{
													ticks: {
														autoSkip: false,
														fontSize: 10
													},
													gridLines: {
														display: false
													},
													stacked: true
												}]
											},
											legend: {
												display: false
											}
										}
									}
								},
								mounted: function() {
									// Add inline plugins
									this.plugins.push(chartDataLabelEnabled);
								}
							}
						},
						template: '<bar-chart :chart-data="chartData.dataCollection" ref="barChart" :width="width" :height="height"></bar-chart>'
					}
				},
                usageDoughnutChart: function (options) {
                    return {
                        // name : 'usage-chart',
                        props: {
                            usageChartData: Object,
                            width: Number,
                            height: Number
                        },
                        data: function () {
                            return {
                                chartData: Object
                            }
                        },
                        created: function() {
                            this.$watch("usageChartData", function(newValue, orgValue) {
                                this.$set(this, "chartData", newValue);
                            }, {
                                deep:true
                            });
                        },
                        components: {
                            "doughnut-chart": {
                                extends: VueChartJs.Doughnut,
                                mixins: [VueChartJs.mixins.reactiveProp],
                                props: { },
                                watch: {
                                    chartData: function () {
                                        var value = this.$data._chart.data.datasets[0].data[0];
                                        this.setCenterText(value);
                                    }
                                },
                                data: function () {
                                    return {
                                        options: (options !== undefined && options !== null) ? options : {
                                            responsive: false,
                                            maintainAspectRatio: false,
                                            elements: {
                                                center: {
                                                    text:0,
                                                    color: '#444', // Default is #000000
                                                    fontStyle: 'Arial', // Default is Arial
                                                    sidePadding: 20 // Defualt is 20 (as a percentage)
                                                }
                                            },
                                            legend : {
                                                display : false
                                            },
                                            tooltips: {
                                                enabled: false,
                                                mode: 'single'
                                            },
                                            cutoutPercentage : 85 // 도넛 차트 두께 설정
                                        }
                                    }
                                },
                                methods:{
                                    setCenterText : function (centerText) {
                                        this.$data._chart.options.elements.center.text = centerText + '%';
                                        this.$data._chart.update();
                                    }
                                }
                            }
                        },
                        template: '<doughnut-chart :chart-data="chartData.dataCollection" :width="width" :height="height"></doughnut-chart>'
                    }
                },
				diskUsageDoughnutChart: function (options) {
					return {
						// name : 'usage-chart',
						props: {
							usageChartData: Object,
							width: Number,
							height: Number
						},
						data: function () {
							return {
								chartData: Object
							}
						},
						created: function() {
							this.$watch("usageChartData", function(newValue, orgValue) {
								this.$set(this, "chartData", newValue);
							}, {
								deep:true
							});
						},
						components: {
							"doughnut-chart": {
								extends: VueChartJs.Doughnut,
								mixins: [VueChartJs.mixins.reactiveProp],
								props: { },
								watch: {
									chartData: function () {
										var value = this.$data._chart.data.datasets[0].data[0];
										this.setText(value);
									}
								},
								data: function () {
									return {
										options: (options !== undefined && options !== null) ? options : {
											responsive: false,
											maintainAspectRatio: false,
											elements: {
												center: {
													text:0,
													color: '#444', // Default is #000000
													fontStyle: 'Arial', // Default is Arial
													sidePadding: 20 // Defualt is 20 (as a percentage)
												}
											},
											legend : {
												display : false
											},
											tooltips: {
												enabled: false,
												mode: 'single'
											},
											cutoutPercentage : 85 // 도넛 차트 두께 설정
										}
									}
								},
								methods:{
									setText : function (centerText) {
										this.$data._chart.options.title.position = "bottom";
										this.$data._chart.options.title.display = true;
										this.$data._chart.options.title.text = this.$data._chart.data.labels[0];
										this.$data._chart.options.title.padding = 0;
										this.$data._chart.options.title.weight = 0;
										this.$data._chart.options.title.lineHeight = 0.6;

										this.$data._chart.options.elements.center.text = centerText + '%';
										this.$data._chart.update();
									}
								}
							}
						},
						template: '<doughnut-chart :chart-data="chartData.dataCollection" :width="width" :height="height"></doughnut-chart>'
					}
				}
			};
		})();

var chartDataLabelEnabled = {
	// Set dynamic Max Value
	beforeInit: function(chart) {
		var datasets = chart.data.datasets,
			maxValue = Number.MIN_VALUE,
			values = [];

		datasets[0].data.forEach(function() {
			values.push(0);
		});

		datasets.forEach(function(dataset) {
			dataset.data.forEach(function(d, j) {
				values[j] += d;
			});
		});

		values.forEach(function(v) {
			(maxValue < v) && (maxValue = v);
		});

		chart.options.scales.xAxes.forEach(function(x) {
			x.ticks.suggestedMax = maxValue + 5;
		});
	},
	// Set labels on bar chart
	afterDatasetsDraw: function(chart) {
		var ctx = chart.ctx,
			labelDataList = [],
			fontSize = 10;

		for (var i = 0, l = chart.data.datasets[0].data.length; i < l; i++) {
			labelDataList.push({pos: {x: 0, y: 0}, value: 0});
		}

		chart.data.datasets.forEach(function (dataset, i) {
			var meta = chart.getDatasetMeta(i);

			if (!meta.hidden) {
				meta.data.forEach(function(element, index) {
					var value = dataset.data[index],
						position = element.tooltipPosition(),
						pos = labelDataList[index].pos,
						x = position.x + 3,
						y = position.y - (fontSize / 2);

					(pos.x < x) && (pos.x = x);
					(pos.y < y) && (pos.y = y);

					labelDataList[index].value += value;
				});
			}
		});

		labelDataList.forEach(function(data) {
			var value = data.value,
				position = data.pos,
				fontStyle = 'normal',
				fontFamily = 'sans-serif';

			ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
			ctx.textAlign = 'left';
			ctx.fillStyle = '#6d6d6d';
			ctx.textBaseline = 'top';

			ctx.fillText(value, position.x, position.y);
		});
	}
};
