/**
 * NFV-O Tosca
 * @type {NFVOTosca}
 */
var NFVOTosca = (function() {

    /**
     * @param {{generalInfo, components}} data
     * @constructor
     */
    function NFVOTosca(data) {
        var template = Template(data);

        assignComponentTemplate(template.topology_template, data.components);

        Object.defineProperties(this, {
            template: {
                get: function() {
                    return template;
                }
            }
        });
    }

    NFVOTosca.prototype = {
        /**
         * return YAML data
         * @returns {string}
         */
        getYAML: function() {
            var convertResult = new YamlConverter(this.template).convert(),
                sentences = convertResult.split('\n'),
                i = 0,
                replaced = '';

            for (; i < sentences.length; i++) {
                var sentence = sentences[i].trim();

                if (sentence.includes('description')) {
                    break;
                }

                if (sentence.includes('-')) {
                    sentence = ' ' + sentence;
                }

                replaced += sentence + '\n';
            }

            for (; i < sentences.length; i++) {
                replaced += sentences[i] + '\n';
            }

            return replaced;
        }
    };

    function Template(data) {
        var generalInfo = data.generalInfo,
            components = data.components;

        return {
            imports: (function(comps) {
                var ret = [];

                for (var i = 0; i < comps.length; i++) {
                    var com = comps[i];

                    if (com.userData.type > 1) {
                        continue;
                    }

                    if (!com.userData.vnf_ins_id) {
                        ret.push(qouto('VNF_PACKAGE:' +  com.userData.pkg_id));
                    } else {
                        ret.push(qouto('VNF:' + com.userData.vnf_ins_id));
                    }
                }

                return ret.length > 0 ? ret : null;
            })(components),
            description: generalInfo.nsdDesc ? qouto(generalInfo.nsdDesc) : '""',
            metadata: {
                ID: qouto(generalInfo.nsdId) || '""' ,
                vendor: qouto(generalInfo.designer),
                version: generalInfo.version ? qouto(generalInfo.version) : '""'
            },
            topology_template: {
                inputs: {
                    flavor_ID: null // todo: 정확한 정보 확인 후 변경
                },
                Groups: {} // todo: 정확한 정보 확인 후 변경
            },
            tosca_default_namespace: null, // todo: 정확한 정보 확인 후 변경
            tosca_definitions_version: qouto('tosca_simple_profile_for_nfv_1_0')  // todo: 정확한 정보 확인 후 변경
        };
    }

    function assignComponentTemplate(assignObj, components) {
        var template = { };

        for (var i = 0; i < components.length; i++) {
            var component = components[i];

            if (component.userData.type > 1) {
                continue;
            }

            template[component.label.text] = {
                capabilities: { // todo: 정확한 정보 확인 후 변경
                    forwarder1: null
                },
                properties: {
                    import: (function(com) {
                        if (!com.userData.vnf_ins_id) {
                            return qouto('VNF_PACKAGE:' +  com.userData.pkg_id);
                        } else {
                            return qouto('VNF:' + com.userData.vnf_ins_id);
                        }
                    })(component)
                },
                requirements: {
                    virtualLink: null
                },
                type: 'tosca.nodes.nfv.VNF.' + component.userData.id,
                _name: qouto(component.label.text)
            };
        }

        Object.assign(assignObj, template);
    }

    function setVlConnections(data) {
        var components = data.components,
            generalInfo = data.generalInfo,
            compObj = {},
            connections = [],
            i;

        for (i = 0; i < components.length; i++) {
            var comp = components[i];

            if (comp.type === 'draw2d.Connection') {
                connections.push(comp);
            } else {
                if (!comp.requirements) {
                    comp.requirements = [];
                }

                compObj[comp.id] = comp;
            }
        }

        for (i = 0; i < connections.length; i++) {
            var conn = connections[i],
                src = compObj[conn.source.node],
                target = compObj[conn.target.node];

            if (Number(src.userData.type) !== 4) {
                src.requirements.push({virtual_link: target.label.text});
            } else {
                target.requirements.push({virtual_link: src.label.text});
            }
        }

        return {
            components: (function(o) {
                var keys = Object.keys(o),
                    ret = [];

                for (var i = 0; i < keys.length; i++) {
                    var k = keys[i],
                        comp = o[k];

                    ret.push(comp);
                }

                return ret;
            })(compObj),
            generalInfo: generalInfo
        };
    }

    function qouto(str) {
        if (!/[\[|\]|{|}|>|\||\*|&|!|%|#|`|@|,|\?|:|\-]/g.test(str)) {
            return '"' + str + '"';
        }

        return str;
    }

    return NFVOTosca;
})();
