var Topology = (function() {
    'use strict';

    var _ns = 'http://www.w3.org/2000/svg';

    var STATUS_EVT = 'changeStatus';

    var TOOLTIP_SHOW_EVT = 'showTooltip';

    var TOOLTIP_HIDE_EVT = 'hideTooltip';

    /**
     * Topology Common Utils
     */
    var util = {
        /**
         * @param {SVGElement} o
         * @param {*} p
         */
        attr: function(o, p) {
            Object.keys(p).forEach(function(n) { o.setAttribute(n, p[n]) });
        },
        /**
         * @param {string} name
         * @param {*?} attr
         * @returns {SVGElement}
         */
        create: function(name, attr) {
            var element = document.createElementNS(_ns, name);

            if (attr) {
                util.attr(element, attr);
            }

            return element;
        },
        /**
         * @param {{
         *     x: number,
         *     y: number,
         *     w: number,
         *     h: number
         * }} vBox
         * @return {string}
         */
        viewBoxToString: function(vBox) {
            return vBox.x + ',' + vBox.y + ',' + vBox.w + ',' + vBox.h;
        },
        getTranslateValue: function(attr) {
            var exec = /translate\(\s*(\d+)\s*,\s*(\d+)\s*\)/gi.exec(attr);

            if (exec != null) {
                return {
                    x: exec[1],
                    y: exec[2]
                }
            }

            return null;
        },
        /**
         * @param {SVGElement} svg,
         * @returns {*}
         */
        getViewBox: function(svg) {
            var vbox = svg.getAttribute('viewBox'),
                vboxArr = vbox.split(','),
                x = parseInt(vboxArr[0].trim()),
                y = parseInt(vboxArr[1].trim()),
                w = parseInt(vboxArr[2].trim()),
                h = parseInt(vboxArr[3].trim());

            return { x: x, y: y,  w: w, h: h };
        },
        /**
         * @param {SVGElement} target
         * @param {SVGGElement} g
         * @returns {*}
         */
        getPosition: function(target, g) {
            var viewBox = util.getViewBox(target),
                svgRect = target.getBoundingClientRect(),
                gRect = g.getBoundingClientRect(),
                top = gRect.top - svgRect.top + viewBox.y,
                left = gRect.left - svgRect.left + viewBox.x,
                width = gRect.width,
                height = gRect.height,
                right = left + width,
                bottom = top + height;

            return {
                top: top,
                left: left,
                right: right,
                bottom: bottom,
                width: width,
                height: height
            }
        },
        moveElement: function(element, x, y) {
            util.attr(element, { transform: 'translate(' + x + ',' + y + ')' });
        },
        calcSeverity: function(components) {
            var min = Number.MAX_VALUE,
                status = null;

            components.forEach(function(component) {
                var _status = component.status;

                if (min > _status.severity) {
                    min = _status.severity;
                    status = _status;
                }
            });

            return status;
        },
        getTypeByName: function(name) {
            var _name = name.toUpperCase(),
                type = Type[_name];

            return type ? type : null;
        },
        getStatusBySeverity: function(severity) {
            if (severity == null) {
                return null;
            }

            var names = Object.keys(Status),
                _severity = parseInt(severity);

            for (var i = 0; i < names.length; i++) {
                var name = names[i],
                    status = Status[name];

                if (status.severity === _severity) {
                    return status;
                }
            }

            return null;
        },
        createStyle: function(styles) {
            var styleNode = document.createElement('style'),
                styleText = '.tp-comp:hover{}';

            Object.keys(styles).forEach(function(cls) {
                var style = styles[cls];

                styleText += '.' + cls + '{';

                Object.keys(style).forEach(function(s) {
                    styleText += s + ':' + style[s] + ';';
                });

                styleText += '}';
            });

            styleNode.appendChild(document.createTextNode(styleText));

            return styleNode;
        },
        randomId: function() {
            var seed = Date.now();

            if (window.performance && typeof window.performance.now === 'function') {
                seed += performance.now();
            }

            var randomId = 'xxxxx4xxxyxxxxx'.replace(/[xy]/g, function(c) {
                var r = (seed + Math.random() * 16) % 16 | 0;

                seed = Math.floor(seed / 16);

                return (c === 'x' ? r: r & (0x3 | 0x8).toString(16));
            });

            return btoa(randomId).replace(/[=]/g, '');
        },
        exportChild: function(children, type) {
            var ret = [];

            Object.values(children).forEach(function(child) {
                if (child instanceof type) {
                    ret.push(child);
                }
            });

            return ret;
        },
        exportComponents: function(components, type) {
            var ret = [];

            Object.values(components).forEach(function(component) {
                if (component.type === type) {
                    ret.push(component);
                }
            });

            return ret;
        }
    };

    /**
     * Use colors
     */
    var colors = {
        gray: '#c4d0de',
        darkblue: '#38485f'
    };

    /**
     * Default settings
     */
    var defaults = {
        link: function(x1, y1, x2, y2, status) {
            return { class: 'vl', x1: x1, y1: y1, x2: x2, y2: y2, stroke: status ? status.color : colors.gray };
        },
        label: function(dx, x, dy, fill) {
            return { class: 'txt', dx: dx, x: x, dy: dy, fill: fill ? fill : '#000' };
        },
        txtLbl: function(dx, x, dy, fill) {
            return { class: 'lblTxt', dx: dx, x: x, dy: dy, fill: fill ? fill : '#000' };
        },
        join: function(status) {
            return { class: 'conn', cx: 0, cy: 9, r: 4, stroke: status ? status.color : colors.darkblue };
        },
        line: function(status) {
            return defaults.link(5, 9, 17, 9, status);
        },
        componentLabel: function() {
            return defaults.txtLbl(16, 17, 48);
        },
        cloudLabel: function() {
            return defaults.label(3, 30, 15, '#fff');
        },
        edgeLabel: function() {
            return defaults.label(3, 30, 70, '#fff');
        },
        image: function(url) {
            return { x: 15, y: 0, href: url, width: 35, height: 35 };
        },
        tooltipTag: function() {
            return { fill: '#b1b1b1', rx: 5,  ry: 5 };
        },
        cloudNameTag: function() {
            return { x: 3, y: 0,  width: 60, height: 20, rx: 10, ry: 10, fill: colors.darkblue };
        },
        edgeNameTag: function() {
            return { x: 3, y: 55,  width: 60, height: 20, rx: 10, ry: 10, fill: status ? status.color : colors.darkblue };
        }
    };

    var properties = {
        domain: {
            offsetX: 230,
            offsetY: 0,
            marginX: 150
        },
        cloud: {
            marginX: 110,
            offsetY: 100
        },
        edge: {
            marginX: 50,
            offsetY: 270
        },
        component: {
            marginX: 67,
            AMF: {
                offsetY: 30
            },
            SMF: {
                offsetY: 100
            },
            ETC: {
                offsetY: 100
            }
        },
        background: {
            offsetX: 21,
            offsetY: 23,
            marginY: 20,
            color: '#f8f9fb',
            txtColor: '#3a4960',
            common: {
                width: 1485,
                height: 50
            },
            central: {
                width: 1485,
                height: 146
            },
            edge: {
                width: 1485,
                height: 100
            }
        }
    };

    /**
     * Device Status
     */
    var Status = {
        CRITICAL: { name: 'CRITICAL', severity: 0, color: '#ed2728', text: '#fff' },
        MAJOR: { name: 'MAJOR', severity: 1, color: '#ff7814', text: '#fff'},
        MINOR: { name: 'MINOR', severity: 2, color: '#f2ef08', text: '#000'},
        WARNING: { name: 'WARNING', severity: 3, color: '#0897f2', text: '#fff' },
        INDETERMINATE: { name: 'INDETERMINATE', severity: 4, color: '#eea5a6', text: '#fff' },
        CLEAR: { name: 'CLEAR', severity: 5, color: '#12a935', text: '#fff' },
        EVENT: { name: 'EVENT', severity: 6, color: '#babbbe', text: '#fff' }
    };

    /**
     * Device Type
     */
    var Type = {
        AMF: { name: 'AMF', url: '/resources/images/contents/ico_amf_single.png', url2: '/resources/images/contents/ico_amf.png' },
        SMF: { name: 'SMF', url: '/resources/images/contents/ico_smf_single.png', url2: '/resources/images/contents/ico_smf.png' },
        UPF: { name: 'UPF', url: '/resources/images/contents/ico_upf_single.png', url2: '/resources/images/contents/ico_upf.png' },
        PCF: { name: 'PCF', url: '/resources/images/contents/ico_com.png' },
        UDM: { name: 'UDM', url: '/resources/images/contents/ico_com.png' }
    };

    /**
     * @Type Topology
     * @constructor
     */
    function Topology(args) {
        var ctx = this,
            _target = args.target instanceof HTMLElement ? args.target
                                                         : document.getElementById(args.target),
            _storage = {
                components: {},
                connections: {}
            },
            _domains = {},
            targetSize = _target.getBoundingClientRect(),
            _svg = util.create('svg', {
                width: targetSize.width,
                height: targetSize.height,
                xmlns: _ns,
                viewBox: '0,0,' + targetSize.width + ',' + targetSize.height,
                // preserveAspectRatio: 'xMidYMid meet'
                preserveAspectRatio: 'xMinYMin meet'
            }),
            _connGrp = util.create('g', {title: 'Connection group'}),
            _compGrp = util.create('g', {title: 'Component group'}),
            _tooltipGrp = util.create('g', {title: 'Tooltip group'}),
            _tooltipBg = util.create('rect', defaults.tooltipTag()),
            _tooltipTxt = util.create('text', {class: 'txt', y: '5'});

        _tooltipGrp.append(_tooltipBg, _tooltipTxt);

        Object.defineProperties(this, {
            storage: {
                set: function() {
                    _storage = {
                        components: {},
                        connections: {}
                    };
                },
                get: function() {
                    return _storage;
                }
            },
            domains: {
                set: function() {
                    _domains = {};
                },
                get: function() {
                    return _domains;
                }
            },
            svg: {
                get: function() {
                    return _svg;
                }
            },
            connectionGroup: {
                get: function() {
                    return _connGrp;
                }
            },
            componentGroup: {
                get: function() {
                    return _compGrp;
                }
            },
            tooltip: {
                get: function() {
                    return {
                        group: _tooltipGrp,
                        background: _tooltipBg,
                        text: _tooltipTxt
                    };
                }
            },
            target: {
                get: function() {
                    return _target;
                }
            }
        });

        _tooltipGrp.style.display = 'none';

        _tooltipGrp.addEventListener('mouseover', function() {
            _tooltipGrp.style.display = '';
        });

        _tooltipGrp.addEventListener('mouseleave', function() {
            _tooltipGrp.style.display = 'none';
        });

        var tooltipOffsetX = 20;

        _svg.addEventListener(TOOLTIP_SHOW_EVT, function(e) {
            var component = e.detail;

            if (component.tooltip) {
                _tooltipGrp.style.display = '';
                ctx.tooltip.text.innerHTML = component.tooltip;

                var txtBox = _tooltipTxt.getBBox();
                var compPos = util.getPosition(_svg, component.element);

                util.attr(_tooltipBg, {
                    width: txtBox.width + tooltipOffsetX,
                    height: 20,
                    x: txtBox.x - (tooltipOffsetX / 2),
                    y: txtBox.y
                });
                util.moveElement(_tooltipGrp, compPos.left + (compPos.width / 2) + 10, compPos.bottom);
            }
        });

        _svg.addEventListener(TOOLTIP_HIDE_EVT, function() {
            _tooltipGrp.style.display = 'none';
        });

        this.initialize();
    }

    Topology.prototype = {
        initialize: function() {
            var ctx = this,
                target = this.target,
                svg = this.svg,
                tooltipGrp = ctx.tooltip.group;

            target.innerHTML = '';
            svg.innerHTML = '';

            svg.appendChild(util.createStyle({
                txt: {
                    'font-size': '13px',
                    'font-family': 'sans-serif',
                    'font-weight': 'bold',
                    'text-anchor': 'middle'
                },
                bgTxt: {
                    'font-size': '14px',
                    'font-family': 'sans-serif',
                    'font-weight': 'bold'
                },
                lblTxt: {
                    'font-size': '10px',
                    'font-family': 'sans-serif',
                    'font-weight': 'bold',
                    'text-anchor': 'middle'
                },
                vl: {
                    'stroke-width': 1.7
                },
                conn: {
                    'stroke-width': 3,
                    'fill': '#fff'
                },
                commons: {
                    'font-size': '17px',
                    'font-weight': 'bold',
                    'fill': colors.darkblue
                }
            }));

            ctx.connectionGroup.innerHTML = '';
            ctx.componentGroup.innerHTML = '';

            svg.append(new Background().element);
            svg.append(ctx.connectionGroup);
            svg.append(ctx.componentGroup);
            svg.append(tooltipGrp);

            target.appendChild(svg);
        },
        /**
         * @param {string} id
         * @returns {boolean}
         */
        hasId: function (id) {
            return !!this.storage[id];
        },
        update: function() {
            var svg = this.svg,
                target = this.target;

            util.attr(svg, {
                width: target.offsetWidth,
                height: target.offsetHeight,
                viewBox: '0,0,' + target.offsetWidth + ',' + target.offsetHeight
            });
        },
        clear: function() {
            this.storage = {};
            this.domains = {};

            this.svg.innerHTML = '';
            this.initialize();
        },
        /**
         * @param {Component} comp
         */
        attachComponent: function(comp) {
            this.componentGroup.append(comp.element);
        },
        /**
         * @param {*} args
         * @returns {Domain}
         */
        createDomain: function (args) {
            var domain = new Domain(args),
                domainId = domain.id,
                oldDomain = this.domains[domainId];

            if (oldDomain != null) {
                oldDomain.update(domain);

                return oldDomain;
            }

            return domain;
        },
        /**
         * @param {*} args
         * @returns {Cloud}
         */
        createCloud: function (args) {
            return new Cloud(args);
        },
        /**
         * @param {*} args
         * @returns {Edge}
         */
        createEdge: function (args) {
            return new Edge(args);
        },
        /**
         * @param {*} args
         * @returns {Component}
         */
        createComponent: function (args) {
            var ctx = this,
                comp = new Component(args),
                id = comp.id,
                ids = id.split(',');

            ids.forEach(function (_id) {
                var compId = _id.trim(),
                    oldComponent = ctx.storage.components[compId];

                if (oldComponent == null) {
                    ctx.storage.components[compId] = comp;
                } else {
                    oldComponent.status = comp.status.severity;
                }
            });

            return comp;
        },
        /**
         * @param {Component} src
         * @param {Component} dst
         * @param {string} id
         * @param {number?} status
         */
        connectComponent: function (src, dst, id, status) {
            if (src.type === dst.type) {
                return;
            }

            var connection = new Connection({
                    src: src,
                    dst: dst,
                    id: id,
                    status: status
                }),
                _id = connection.id,
                connectionFlag = true,
                oldConnection = this.storage.connections[_id];

            // Find source relations
            if (!src.association.some(function(tuple) {
                if (tuple[0] === dst) {
                    connectionFlag = false;

                    return true;
                }

                return false;
            })) {
                src.association.push([dst, connection]);
            }

            // Find target relations
            if (!dst.association.some(function(tuple) {
                if (tuple[0] === src) {
                    connectionFlag = false;

                    return true;
                }

                return false;
            })) {
                dst.association.push([src, connection]);
            }

            if (oldConnection == null) {
                this.storage.connections[_id] = connection;

                if (connectionFlag) {
                    this.connectionGroup.append(connection.element);
                }
            } else {
                var newStatus = connection.status;

                if (newStatus != null) {
                    oldConnection.status = newStatus.severity;
                }
            }
        },
        /**
         * @param {string} id
         * @returns {Component}
         */
        getComponent: function (id) {
            var component = this.storage.components[id];

            return component ? component : null;
        },
        getConnection: function(id) {
            var connection = this.storage.connections[id];

            return connection ? connection : null;
        },
        /**
         * @param {number|string} severity
         * @returns {*}
         */
        getStatusBySeverity: function (severity) {
            var _severity = parseInt(severity);

            for (var n in Status) {
                if (Status.hasOwnProperty(n)) {
                    var status = Status[n];

                    if (status.severity === _severity) {
                        return status;
                    }
                }
            }

            return null;
        },
        /**
         * @param {Array<Domain>} domains
         */
        setDomains: function (domains) {
            var ctx = this,
                compGrp = ctx.componentGroup,
                offsetX = properties.domain.offsetX,
                marginX = properties.domain.marginX;

            domains.forEach(function (domain) {
                var id = domain.id,
                    oldDomain = ctx.domains[id];

                if (oldDomain != null) {
                    oldDomain.update(domain);
                } else {
                    var domainElement = domain.element,
                        position = domain.position;

                    compGrp.append(domainElement);

                    domain.attached = true;

                    if (position == null) {
                        domain.move(offsetX, properties.domain.offsetY);

                        offsetX += domainElement.getBBox().width + marginX;
                    } else {
                        domain.move(position.x, position.y);
                    }

                    ctx.domains[id] = domain;
                }
            });
        },
        getDefaultsProperties: function () {
            return properties;
        },
        removeComponent: function(id) {
            var component = this.getComponent(id);

            if (component != null) {
                var element = component.element,
                    parent = element.parentElement,
                    compStorage = this.storage.components,
                    connStorage = this.storage.connections;

                delete compStorage[id];

                parent.removeChild(element);

                Object.keys(connStorage).forEach(function(connId) {
                    var connection = connStorage[connId];

                    if (connection.src.id.includes(id) || connection.dst.id.includes(id)) {
                        var connectionElement = connection.element;

                        delete connStorage[connId];

                        connectionElement.parentElement.removeChild(connectionElement);
                    }
                });
            }
        },
        addWheelEvent: function() {
            var svg = this.svg;

            svg.addEventListener('wheel', function(e) {
                e.preventDefault();

                var vbox = util.getViewBox(svg),
                    w = vbox.w + (e.deltaY / 3),
                    h = vbox.h + (e.deltaX / 3);

                (w <= 0) && (w = 1);
                (h <= 0) && (h = 1);

                vbox.w = w;
                vbox.h = 1;


                util.attr(svg, { viewBox: util.viewBoxToString(vbox) });
            });
        }
    };

    /**
     * @type Connection
     */
    var Connection = (function() {

        function Connection(args) {
            var _status = util.getStatusBySeverity(args.status),
                _src = args.src,
                _dst = args.dst,
                _id = args.id,
                srcPosition = _src.viewPosition,
                dstPosition = _dst.viewPosition,
                line = util.create('line', defaults.link(
                    0,
                    0,
                    dstPosition.x - srcPosition.x,
                    dstPosition.y - srcPosition.y
                ));

            util.moveElement(line, srcPosition.x, srcPosition.y);

            Object.defineProperties(this, {
                id: {
                    get: function() {
                        return _id;
                    }
                },
                src: {
                    get: function() {
                        return _src;
                    }
                },
                dst: {
                    get: function() {
                        return _dst;
                    }
                },
                status: {
                    set: function(__status) {
                        _status = util.getStatusBySeverity(__status);

                        if (_status != null) {
                            util.attr(line, { stroke: _status.color });
                        }
                    },
                    get: function() {
                        return _status;
                    }
                },
                element: {
                    get: function() {
                        return line;
                    }
                }
            });

            line.id = _id;

            if (_status != null) {
                util.attr(line, { stroke: _status.color });
            }
        }

        return Connection;
    })();

    /**
     * @type Component
     */
    var Component = (function() {

        /**
         * @constructor
         */
        function Component(args) {
            var ctx = this,
                group = util.create('g'),
                groupStyle = group.style,
                _id = args.id,
                _name = args.name,
                _tooltip = args.tooltip,
                _labelName = _name.length > 8 ? _name.substring(_name.length - 8) : _name,
                _type = util.getTypeByName(args.type),
                _status = util.getStatusBySeverity(args.status) || Status.CLEAR,
                _url = (function() {
                    if (args.url) {

                        return args.url;
                    } else if (args.isGroup === true) {

                        return _type.url2;
                    } else {
                        var ids = _id.split(',');

                        if (ids.length > 1) {
                            return _type.url2;
                        }
                    }

                    return _type.url;
                })(),
                _position = args.position,
                _viewPosition = { x: defaults.join(_status).cx, y: defaults.join(_status).cy },
                _association = [],
                _fnTooltip = null,
                line = util.create('line', defaults.line(null)),
                join = util.create('circle', defaults.join(_status)),
                image = util.create('image', defaults.image(_url)),
                label = util.create('text', defaults.componentLabel()),
                labelStyle = label.style;

            if (args.isCommon === true) {
                util.attr(image, { y: -8 });

                util.attr(label, { class: 'commons', dx: 40, x: 15, y: 15, dy: 0 });
            }

            Object.defineProperties(this, {
                id: {
                    get: function() {
                        return _id;
                    }
                },
                name: {
                    set: function(__name) {
                        _name = __name;
                    },
                    get: function() {
                        return _name;
                    }
                },
                type: {
                    get: function() {
                        return _type;
                    }
                },
                status: {
                    set: function(__status) {
                        _status = util.getStatusBySeverity(__status);

                        if (_status != null) {
                            util.attr(join, { stroke: _status.color });

                            group.dispatchEvent(new CustomEvent(STATUS_EVT, { bubbles: true }));
                        }
                    },
                    get: function() {
                        return _status;
                    }
                },
                position: {
                    set: function(__position) {
                        _position = __position;

                        ctx.move(_position.x, _position.y);
                    },
                    get: function() {
                        return _position;
                    }
                },
                viewPosition: {
                    get: function() {
                        return _viewPosition;
                    }
                },
                element: {
                    get: function() {
                        return group;
                    }
                },
                association: {
                    get: function() {
                        return _association;
                    }
                },
                connector: {
                    get: function() {
                        return join;
                    }
                },
                clickEvent: {
                    set: function(fn) {
                        if (fn instanceof Function) {
                            group.addEventListener('click', fn);
                            groupStyle.cursor = 'pointer';
                        }
                    }
                },
                tooltip: {
                    get: function() {
                        return _tooltip;
                    }
                }
            });

            group.id = _id;

            label.innerHTML = _labelName;
            labelStyle.display = '';

            group.append( line, join, image, label);

            if (args.isCommon !== true) {
                group.addEventListener('mouseover', function () {
                    group.dispatchEvent(new CustomEvent(TOOLTIP_SHOW_EVT, { bubbles: true, detail: ctx }));
                });

                group.addEventListener('mouseleave', function () {
                    group.dispatchEvent(new CustomEvent(TOOLTIP_HIDE_EVT, { bubbles: true }));
                });
            }

            if (_position) {
                this.move(_position.x, _position.y);
            }
        }

        Component.prototype = {
            move: function(x, y) {
                var ctx = this;

                var orgPos = util.getTranslateValue(ctx.element.getAttribute('transform'));

                if (orgPos == null) {
                    orgPos = { x: 0, y: 0};
                }

                util.moveElement(ctx.element, x, y);

                var xDiff = x - orgPos.x,
                    yDiff = y - orgPos.y;

                ctx.viewPosition.x += xDiff;
                ctx.viewPosition.y += yDiff;
            },
            /**
             * @param {Component} component
             */
            update: function(component) {
                if (this.id === component.id) {
                    this.name = component.name;
                    this.status = component.status.severity;
                }
            }
        };

        return Component;
    })();

    /**
     * @type Cloud
     */
    var Cloud = (function() {

        function Cloud(args) {
            var ctx = this,
                _id = args.id,
                _components = {},
                _name = args.name,
                _status = null,
                componentList = args.components,
                group = util.create('g'),
                label = util.create('text', defaults.cloudLabel()),
                nameTag = util.create('rect', defaults.cloudNameTag());

            Object.defineProperties(this, {
                id : {
                    get: function() {
                        return _id;
                    }
                },
                status: {
                    set: function(__status) {
                        _status = util.getStatusBySeverity(__status);

                        if (_status != null) {
                            util.attr(nameTag, { fill: _status.color });
                            util.attr(label, {fill: _status.text});
                        }
                    },
                    get: function() {
                        return _status;
                    }
                },
                element: {
                    get: function() {
                        return group;
                    }
                },
                components: {
                    get: function() {
                        return _components;
                    }
                },
                AMFs: {
                    get: function() {
                        return util.exportComponents(_components, Type.AMF);
                    }
                },
                SMFs: {
                    get: function() {
                        return util.exportComponents(_components, Type.SMF);
                    }
                }
            });

            if (Array.isArray(componentList)) {
                var offsetXAMF = 0,
                    offsetXSMF = 0,
                    offsetYAMF = properties.component.AMF.offsetY,
                    offsetYSMF = properties.component.SMF.offsetY;

                componentList.forEach(function(component) {
                    var id = component.id,
                        oldComponent = _components[id];

                    if (oldComponent == null) {
                        _components[id] = component;

                        switch (component.type) {
                            case Type.AMF:
                                component.move(offsetXAMF, offsetYAMF);

                                offsetXAMF += properties.component.marginX;
                                break;
                            case Type.SMF:
                                component.move(offsetXSMF, offsetYSMF);

                                offsetXSMF += properties.component.marginX;
                                break;
                        }
                    } else {
                        oldComponent.update(component);
                    }

                    group.append(component.element);
                });
            }

            label.innerHTML = _name;

            var severity = util.calcSeverity(Object.values(ctx.components));

            if (severity != null) {
                ctx.status = severity.severity;
            } else {
                ctx.status = 5;
            }

            group.append(nameTag, label);

            group.addEventListener(STATUS_EVT, function() {
                ctx.status = util.calcSeverity(Object.values(ctx.components)).severity;
            });
        }

        Cloud.prototype = {
            move: function(x, y) {
                var ctx = this;

                var orgPos = util.getTranslateValue(ctx.element.getAttribute('transform'));

                if (orgPos == null) {
                    orgPos = { x: 0, y: 0};
                }

                util.moveElement(ctx.element, x, y);

                Object.values(ctx.components).forEach(function(component) {
                    var xDiff = x - orgPos.x,
                        yDiff = y - orgPos.y;

                    component.viewPosition.x += xDiff;
                    component.viewPosition.y += yDiff;
                });
            },
            update: function(cloud) {
                var ctx = this;

                if (ctx.id === cloud.id) {
                    Object.values(cloud.components).forEach(function(component) {
                        var id = component.id,
                            oldComponent = ctx.components[id];

                        if (oldComponent != null) {
                            oldComponent.update(component);
                        }
                    });
                }
            }
        };

        return Cloud;
    })();

    /**
     * @type Edge
     */
    var Edge = (function() {

        function Edge(args) {
            var ctx = this,
                _components = {},
                _id = args.id,
                _name = args.name,
                _status = null,
                componentList = args.components,
                group = util.create('g'),
                nameTag = util.create('rect', defaults.edgeNameTag(_status)),
                label = util.create('text', defaults.edgeLabel());

            Object.defineProperties(this, {
                id: {
                    get: function() {
                        return _id;
                    }
                },
                name: {
                    get: function() {
                        return _name;
                    }
                },
                status: {
                    set: function(__status) {
                        _status = util.getStatusBySeverity(__status);

                        if (_status != null) {
                            util.attr(nameTag, { fill: _status.color });
                            util.attr(label, {fill: _status.text});
                        }
                    },
                    get: function() {
                        return _status;
                    }
                },
                element: {
                    get: function() {
                        return group;
                    }
                },
                components: {
                    get: function() {
                        return _components;
                    }
                },
                UPFs: {
                    get: function() {
                        return Object.values(_components);
                    }
                }
            });

            if (Array.isArray(componentList)) {
                var offsetXUPF = 0;

                componentList.forEach(function(component) {
                    var id = component.id,
                        oldComponent = _components[id];

                    if (oldComponent == null) {
                        _components[id] = component;

                        component.move(offsetXUPF, 0);

                        offsetXUPF += properties.component.marginX;

                        group.append(component.element);
                    } else {
                        oldComponent.update(component);
                    }
                });
            }

            label.innerHTML = _name;

            ctx.status = util.calcSeverity(ctx.UPFs).severity;

            group.append(nameTag, label);

            group.addEventListener(STATUS_EVT, function() {
                ctx.status = util.calcSeverity(ctx.UPFs).severity;
            });
        }

        Edge.prototype = {
            move: function(x, y) {
                var ctx = this;

                var orgPos = util.getTranslateValue(ctx.element.getAttribute('transform'));

                if (orgPos == null) {
                    orgPos = { x: 0, y: 0};
                }

                util.moveElement(ctx.element, x, y);

                Object.values(ctx.components).forEach(function(component) {
                    var xDiff = x - orgPos.x,
                        yDiff = y - orgPos.y;

                    component.viewPosition.x += xDiff;
                    component.viewPosition.y += yDiff;
                });
            },
            update: function(edge) {
                var ctx = this;

                Object.values(edge.components).forEach(function(component) {
                    var id = component.id,
                        oldComopnent = ctx.components[id];

                    if (oldComopnent != null) {
                        oldComopnent.update(component);
                    }
                });
            }
        };

        return Edge;
    })();

    /**
     * @type Domain
     */
    var Domain = (function() {

        function Domain(args) {
            var _id = args.id,
                _children = {},
                _attached = false,
                _position = args.position,
                children = args.children,
                group = util.create('g');

            Object.defineProperties(this, {
                id: {
                    get: function() {
                        return _id;
                    }
                },
                attached: {
                    set: function(flag) {
                        if (flag === true) {
                            var offsetCloudX = 0,
                                offsetEdgeX = 0,
                                marginCloudX = properties.cloud.marginX,
                                marginEdgeX = properties.edge.marginX,
                                cloudSize = this.clouds.length;

                            Object.keys(_children).forEach(function(id) {
                                var child = _children[id],
                                    childElement = child.element,
                                    childPosition = childElement.getBBox();

                                if (child instanceof Cloud) {
                                    child.move(offsetCloudX, properties.cloud.offsetY);

                                    offsetCloudX += childPosition.width + marginCloudX;
                                } else if (child instanceof Edge) {
                                    /*if (cloudSize > 1) {
                                        // Cloud 사이즈가 여러개일 경우 Edge 는 한개라고 가정
                                        var edgeHalf = child.element.getBBox().width / 2,
                                            domainHalf = group.getBBox().width / 2;

                                        child.move(domainHalf - edgeHalf, properties.edge.offsetY);
                                    } else {
                                        child.move(offsetEdgeX, properties.edge.offsetY);
                                        offsetEdgeX += childPosition.width + marginEdgeX;
                                    }*/
                                    child.move(offsetEdgeX, properties.edge.offsetY);
                                    offsetEdgeX += childPosition.width + marginEdgeX;
                                }
                            });
                        }
                    },
                    get: function() {
                        return _attached;
                    }
                },
                clouds: {
                    get: function() {
                        return util.exportChild(children, Cloud);
                    }
                },
                edges: {
                    get: function() {
                        return util.exportChild(children, Edge);
                    }
                },
                children: {
                    get: function() {
                        return _children;
                    }
                },
                element: {
                    get: function() {
                        return group;
                    }
                },
                position: {
                    get: function() {
                        return _position;
                    }
                }
            });

            if (Array.isArray(children)) {
                children.forEach(function(child) {
                    var childId = child.id,
                        oldChild = _children[childId];

                    if (oldChild == null) {
                        _children[childId] = child;

                        group.append(child.element);
                    } else {
                        oldChild.update(child);
                    }
                });
            }
        }

        Domain.prototype = {
            move: function(x, y) {
                var ctx = this;

                var orgPos = util.getTranslateValue(ctx.element.getAttribute('transform'));

                if (orgPos == null) {
                    orgPos = { x: 0, y: 0};
                }

                util.moveElement(ctx.element, x, y);

                Object.values(ctx.children).forEach(function(child) {
                    Object.values(child.components).forEach(function(comp) {
                        var xDiff = x - orgPos.x,
                            yDiff = y - orgPos.y;

                        comp.viewPosition.x += xDiff;
                        comp.viewPosition.y += yDiff;
                    });
                });
            },
            update: function(domain) {
                var ctx = this;

                Object.values(domain.children).forEach(function(child) {
                    var id = child.id,
                        oldChild = ctx.children[id];

                    if (oldChild != null) {
                        oldChild.update(child);
                    }
                });
            }
        };

        return Domain;
    })();

    /**
     * @type Background
     */
    var Background = (function() {

        /**
         * @constructor
         */
        function Background() {
            var _enabled = false,
                bgInfo = properties.background,
                commInfo = bgInfo.common,
                centreInfo = bgInfo.central,
                edgeInfo = bgInfo.edge,
                rx = 10,
                ry = 10,
                offsetX = bgInfo.offsetX,
                offsetY = bgInfo.offsetY,
                lblOffsetX = bgInfo.offsetX + 20,
                group = util.create('g', {title: 'background_group'}),
                common = util.create('rect', {
                    x: offsetX,
                    y: offsetY,
                    rx: rx,
                    ry: ry,
                    width: commInfo.width,
                    height: commInfo.height,
                    fill: bgInfo.color
                }),
                commLabel = util.create('text', {
                    class: 'bgTxt',
                    x: lblOffsetX,
                    y: bgInfo.offsetY + (commInfo.height / 2) + 7,
                    fill: bgInfo.txtColor
                }),
                central = util.create('rect', {
                    x: offsetX,
                    y: offsetY += commInfo.height + bgInfo.marginY,
                    rx: rx,
                    ry: ry,
                    width: centreInfo.width,
                    height: centreInfo.height,
                    fill: bgInfo.color
                }),
                centreLabel = util.create('text', {
                    class: 'bgTxt',
                    x: lblOffsetX,
                    y: offsetY + (centreInfo.height / 2) + 7,
                    fill: bgInfo.txtColor
                }),
                edge = util.create('rect', {
                    x: offsetX,
                    y: offsetY += centreInfo.height + bgInfo.marginY,
                    rx: rx,
                    ry: ry,
                    width: edgeInfo.width,
                    height: edgeInfo.height,
                    fill: bgInfo.color
                }),
                edgeLabel = util.create('text', {
                    class: 'bgTxt',
                    x: lblOffsetX,
                    y: offsetY + (edgeInfo.height / 2) + 7,
                    fill: bgInfo.txtColor
                });

            commLabel.innerHTML = 'Common';
            centreLabel.innerHTML = 'Central Cloud';
            edgeLabel.innerHTML = 'Edge Cloud';

            group.append(common, central, edge, commLabel, centreLabel, edgeLabel);

            Object.defineProperties(this, {
                enabled: {
                    set: function(flag) {
                        _enabled = flag;
                    },
                    /**
                     * @return {boolean}
                     */
                    get: function() {
                        return _enabled;
                    }
                },
                element: {
                    /**
                     * @return {SVGGElement}
                     */
                    get: function() {
                        return group;
                    }
                }
            });
        }

        /**
         * @method
         */
        Background.prototype = {
            /**
             * @return {SVGGElement}
             */
            getElement: function() {
                return this.element;
            }
        };

        return Background;
    })();

    return Topology;
})();
