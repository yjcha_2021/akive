;(function($) {
	//ajax loading 추가
	$.ajaxSetup({
		beforeSend:function(xhr) {
			xhr.setRequestHeader("AJAX", "true");
			//$("#loadingBar").show();
		},
		complete:function() {
			//$("#loadingBar").hide();
		},
		error:function(xhr, status, error) {
			console.log(xhr.status);
		}
	});

	//jquery validation default setting
	$.validator.setDefaults({
		onkeyup:false,
		onclick:false,
		onfocusout:false,
		ignore:"",
		showErrors:function(errorMap, errorList) {
			if(this.numberOfInvalids()) {
				var errorMsg = "";
				for(var i = 0; i < errorList.length; i++) {
					if(i == 0) {
						errorMsg += errorList[i].message;
					}
				}
				alert(errorMsg);
			}
		}
	});

	$.validator.addMethod("emailFormat", function(value, element) {
		return this.optional(element) || /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/g.test(value);
	});
})($);

var e2eoAlarmVue;
var e2eo = e2eo || (function() {
			return {
				callAjax:function(ajaxData, callback) {
					var resultData = null;

					$.ajax({
						type:ajaxData.type,
						url:ajaxData.url,
						data:ajaxData.data,
						async:e2eo.isEmpty(ajaxData.async) ? true : ajaxData.async,
						traditional:e2eo.isEmpty(ajaxData.traditional) ? true : ajaxData.traditional,
						beforeSend:function(xhr) {
							if(!e2eo.isEmpty(ajaxData.showLoading) && ajaxData.showLoading) {
								$("#loadingBar").show();
							}
						},
						success:function(data) {
							resultData = data;
						},
						error:function(xhr) {
                            //console.log('111111111111111111111111');
							console.log(xhr);
							if (xhr.status == 403) {
                                window.parent.postMessage({"type": "sessionExpire"}, "*");
							}
						},
						complete:function() {
							callback(resultData);
						}
					});
				},
                callJsonAjax:function(ajaxData, callback) {
                    var resultData = null;

                    $.ajax({
                        type:ajaxData.type,
                        contentType:ajaxData.contentType,
                        url:ajaxData.url,
                        data:ajaxData.data,
                        async:e2eo.isEmpty(ajaxData.async) ? true : ajaxData.async,
                        traditional:e2eo.isEmpty(ajaxData.traditional) ? true : ajaxData.traditional,
                        dataType:ajaxData.dataType,
                        beforeSend:function(xhr) {
                            if(!e2eo.isEmpty(ajaxData.showLoading) && ajaxData.showLoading) {
                                $("#loadingBar").show();
                            }
                        },
                        success:function(data) {
                            resultData = data;
                        },
                        error:function(xhr) {
                            console.log(xhr);
                        },
                        complete:function() {
                            callback(resultData);
                        }
                    });
                },
				openPopup:function(selector) {
					var popLeft = (($(window).width() - $(selector).outerWidth()) / 2);
					var popTop = (($(window).height() - $(selector).outerHeight()) / 2);
					if(popLeft < 0) {
						popLeft = 0;
					}
					if(popTop < 0) {
						popTop = 0;
					}
					$("body").addClass("popup_visible");
					$(".popup_mask").fadeIn();
					$(selector).css({"left":popLeft, "top":popTop}).fadeIn().addClass("on").draggable({ cancel: '.pop_con_wrap' });
					$(selector).find("a.pop_close").on("click", function() {
						$(selector).fadeOut().removeClass("on");
						$("body").removeClass("popup_visible");
						$(".popup_mask").fadeOut();
						return false;
					});
					return false;
				},
				closePopup:function(selector) {
					$(selector).fadeOut().removeClass("on");
					$("body").removeClass("popup_visible");
					$(".popup_mask").fadeOut();
					return false;
				},
				getJsonTemplate:function(path, callback) {
					var xobj = new XMLHttpRequest();
					xobj.overrideMimeType("application/json");
					xobj.open("GET", path, true);
					xobj.onreadystatechange = function(ev) {
						if(xobj.readyState == 4 && xobj.status == "200") {
							callback(xobj.responseText);
						}
					};
					xobj.send(null);
				},
				isEmpty:function(obj) {
					if(obj == null || obj == "" || obj == window.undefined) {
						return true;
					} else {
						return false;
					}
				},
				setPhoneNumber:function(number) {
					var regexp = null;
					var format = null;

					if(number.length == 11) {
						regexp = /(\d{3})(\d{4})(\d{4})/g;
						format = "$1-$2-$3";
					} else if(number.length == 8) {
						regexp = /(\d{4})(\d{4})/g;
						format = "$1-$2";
					} else {
						if(number.indexOf("02") == 0) {
							regexp = /(\d{2})(\d{4})(\d{4})/g;
							format = "$1-$2-$3";
						} else {
							regexp = /(\d{3})(\d{3})(\d{4})/g;
							format = "$1-$2-$3";
						}
					}

					return number.replace(regexp, format);
				},
				getFormatDate:function(dateStr, format) {
					if(dateStr == window.undefined || dateStr == "" || dateStr.length < 14) return "";

					var tempDateStr = dateStr.substr(0, 10) + " " + dateStr.substr(-8);

					if(format == "date") {
						return moment(tempDateStr).format("YYYY-MM-DD");
					} else if(format == "time") {
						return moment(tempDateStr).format("HH:mm:ss");
					}
				},
				getUUID:function() {
					return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
						var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
						return v.toString(16);
					});
				},
				setAlarmTerm: function (term) {
					this.callAjax({
						"type": "POST",
						"data": {"alarmTerm": term},
						"url": "/monitoring/common/getAlarmTermDetailAjax"
					}, console.log)
				},
				initAlarmVue: function (
					lastAlarmTimestamp,
					severity,
					perceivedSeverity,
					perceivedSeverityNm,
					siteName,
					objInsName,
					equipType,
					eventTypeNm,
					createdAtFormat,
					alarmRaisedTime,
					extifType,
					probableCause,
					isMute,
					isPause,
					isActive
				) {
					if (lastAlarmTimestamp === undefined || lastAlarmTimestamp === null) {
						lastAlarmTimestamp = 0;
					}
					e2eoAlarmVue = new Vue({
						el: "#alarm-container",
						data: {
							ajaxResultData: null,
							lastTimestamp: lastAlarmTimestamp,
							doAlarm: true,
							alarmTerm: 5000,
							isActive: isActive,
							isShow:false, //Footer Alarm 정보에 확대 버튼을 보일지 말지 (Dashboard 에서만 true)
							isOpen:false, //Dashboard 에서 알람 리스트를 펼쳐 봤는지. 펼치면 True 장애발생 Sound 컨트롤에 사용 > 이전의 doAlarm 대신 사용될 것
							alarmModel:{
								severity:null,
								perceivedSeverity:null,
								perceivedSeverityNm:null,
								siteName:null,
								objInsName:null,
								equipType:null,
								eventTypeNm:null,
								createdAtFormat:null,
								alarmRaisedTime:null,
								extifType:null,
								probableCause:null
							},
							alarmInterval:null,
							isMute : isMute,
							isPause : isPause
						},
						created:function() {
							this.$set(this.alarmModel, "severity", severity);
							this.$set(this.alarmModel, "perceivedSeverity", perceivedSeverity);
							this.$set(this.alarmModel, "perceivedSeverityNm", perceivedSeverityNm);
							this.$set(this.alarmModel, "siteName", siteName);
							this.$set(this.alarmModel, "objInsName", objInsName);
							this.$set(this.alarmModel, "equipType", equipType);
							this.$set(this.alarmModel, "eventTypeNm", eventTypeNm);
							this.$set(this.alarmModel, "createdAtFormat", createdAtFormat);
							this.$set(this.alarmModel, "alarmRaisedTime", alarmRaisedTime);
							this.$set(this.alarmModel, "extifType", extifType);
							this.$set(this.alarmModel, "probableCause", probableCause);
							this.$set(this, "isOpen", false);
							if (location.pathname == "/monitoring/dashboard/site" || location.pathname == "/monitoring/dashboard/main" || location.pathname == "/monitoring/nsMonitoring/main" || location.pathname == "/monitoring/siteMonitoring/main") {
								this.$set(this, "isShow", true);
							} else {
								this.$set(this, "isShow", false);
							}

							this.initSoundBtn();

						},
						mounted: function () {
							this.checkAlarm();
						},
						methods: {
							checkAlarm: function () {
								// Ajax 호출시 Opera Session 갱신
								window.parent.postMessage({"type": "gnbSetMenu", "menuId": ""}, "*");

								var ctx  = this;

								$.ajax({
									type: "POST",
									url: "/monitoring/common/getLastAlarmTimeDetailAjax",
									traditional: true,
									success: function (data) {
										e2eoAlarmVue.alarmTerm = data.optionalData; // Polling 주기 Property 값으로 Setting
										if(data.data != null){ //Alarm Null 체크
											var selectAlarmLastTime = data.data.createdTime;
											if (selectAlarmLastTime != e2eoAlarmVue.lastTimestamp) {
												e2eoAlarmVue.getLastAlarmData(); // 마지막 알람 정보 호출
											}
										} else { //Alarm 정보가 없을 때 데이터 초기화
											e2eoAlarmVue.$set(e2eoAlarmVue.alarmModel, "severity", "");
											e2eoAlarmVue.$set(e2eoAlarmVue.alarmModel, "perceivedSeverityNm", "");
											e2eoAlarmVue.$set(e2eoAlarmVue.alarmModel, "siteName", "");
											e2eoAlarmVue.$set(e2eoAlarmVue.alarmModel, "objInsName", "");
											e2eoAlarmVue.$set(e2eoAlarmVue.alarmModel, "equipType", "");
											e2eoAlarmVue.$set(e2eoAlarmVue.alarmModel, "eventTypeNm", "");
											e2eoAlarmVue.$set(e2eoAlarmVue.alarmModel, "createdAtFormat", "");
											e2eoAlarmVue.$set(e2eoAlarmVue.alarmModel, "alarmRaisedTime", "");
											e2eoAlarmVue.$set(e2eoAlarmVue.alarmModel, "extifType", "");
											e2eoAlarmVue.$set(e2eoAlarmVue.alarmModel, "probableCause", "");
										}

										// String to boolean
										// ex) 'false' -> false
										if (JSON.parse(ctx.isActive)) {
											e2eoAlarmVue.alarmTimeOut();
										}
									},
									error: function (xhr) {
										console.log(xhr);
									}
								});
							},
							alarmTimeOut: function () {
								// alarm 체크 주기 (세션값, 기본 5초)
								this.alarmInterval = setTimeout(function () {
									e2eoAlarmVue.checkAlarm();
								}, this.alarmTerm);
							},
							getLastAlarmData:function () {
								$.ajax({
									type: "POST",
									url: "/monitoring/common/getLastAlarmDetailAjax",
									traditional: true,
									success: function (data) {
										var alarmInfo = data.data;
										// 마지막 알람 타임스탬프 갱신
										e2eoAlarmVue.$set(e2eoAlarmVue, "lastTimestamp", alarmInfo.createdTime);
										// 팝업으로 뿌려질 데이터
										e2eoAlarmVue.$set(e2eoAlarmVue, 'alarmModel', alarmInfo);
										// Footer 마지막 알람 사운드 재생 > 모니터링이 아닌 페이지에서만 나오게, 세션 설정이 Mute 상태가 아닐때만
										if (!e2eoAlarmVue.isShow && !e2eoAlarmVue.isMute) {
											e2eoAlarmVue.setAlarmSound(alarmInfo.severity);
										}
									},
									error: function (xhr) {
										console.log(xhr);
									}
								});
                            },
							setAlarmSound: function (severity) {
								if(severity == 0) {
									if(($("#level_Critical").get(0).error == null) && ($("#level_Critical").get(0) != undefined)) {
										$("#level_Critical").get(0).play().catch(function(err) { });;
									}
								} else if(severity == 1) {
									if(($("#level_Major").get(0).error == null) && ($("#level_Major").get(0) != undefined)) {
										$("#level_Major").get(0).play().catch(function(err) { });;
									}
								} else if(severity == 2) {
									if(($("#level_Minor").get(0).error == null) && ($("#level_Minor").get(0) != undefined)) {
										$("#level_Minor").get(0).play().catch(function(err) { });;
									}
								} else if(severity == 3) {
									if(($("#level_Warning").get(0).error == null) && ($("#level_Warning").get(0) != undefined)) {
										$("#level_Warning").get(0).play().catch(function(err) { });;
									}
								} else if(severity == 4) {
									if(($("#level_Indeterminate").get(0).error == null) && ($("#level_Indeterminate").get(0) != undefined)) {
										$("#level_Indeterminate").get(0).play().catch(function(err) { });;
									}
								}
							},
							getSeverityClass: function(level) {
								if (level == '0') { return "bg-critical"; }
								else if (level == '1') { return "bg-major"; }
								else if (level == '2') { return "bg-minor"; }
								else if (level == '3') { return "bg-warning"; }
								else if (level == '4') { return "bg-indeterminate"; }
								else if (level == '5') { return "bg-clear"; }
								else if (level == '6') { return "bg-event"; }
								else if (level == '10'){ return "bg-clear"; }
								else { return ""; }
							},
							openAlarmList:function () { //Footer 에서 대시보드 페이지에서만 AlarmList 확대 버튼 표시
								if(this.isOpen){
									this.$set(this, "isOpen", false);
								} else {
									this.$set(this, "isOpen", true);
								}
                            },
							goDashboard:function () { //Footer 클릭시 대시보드 페이지로 이동
								if (location.pathname == "/monitoring/dashboard/site" || location.pathname == "/monitoring/dashboard/main") {
									window.parent.postMessage({"type": "goTop"}, "*"); //대시보드에서 선택시 스크롤만 만 제일 위로
								} else {
									confirmModal("Dashboard Alarm", "Dashboard 페이지로 이동 하시겠습니까?", function(){
										location.href='/monitoring/dashboard/site';
										window.parent.postMessage({"type": "gnbSetMenu", "menuId": "M000000017"}, "*"); //메뉴 탭 이동
									},null,"OK","Cancel");
								}
                            },
							initSoundBtn : function () {
								if(isMute){ //눌려진 상태 : true
									$('#btn_sound').toggleClass('off');
									$('#footerSound').toggleClass('off');
								}

								if(isPause){ //눌려진 상태 : true
									$('#btn_pause').toggleClass('off');
									$('#footerPause').toggleClass('off');
								}
                            },
							setAlarmSoundBtn : function () {
								//소리 재생 Flag 설정
								if($('#footerSound').hasClass('off')){
									this.$set(this, "isMute", false);
								} else {
									this.$set(this, "isMute", true);
								}

								//Footer의 버튼 눌렀을 때 버튼 토글
								$('#footerSound').toggleClass('off');

								var tempIsMute = this.isMute;

								//일시정지 상태 Session 설정
								$.ajax({
									type: "POST",
									url: "/monitoring/common/setAlarmSessionAjax",
									traditional: true,
									data:{
										isMute : tempIsMute
									},
									success: function (data) {
										console.log("### Common Session 설정 성공");
									},
									error: function (xhr) {
										console.log(xhr);
									}
								});
                            }
						} //Method End
					});
				},
				getDatePicker:function() {
					$(".select_date").datepicker({
						monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
						dayNames: [ "일", "월", "화", "수", "목", "금", "토" ],
						dayNamesShort: [ "일", "월", "화", "수", "목", "금", "토" ],
						dayNamesMin: [ "일", "월", "화", "수", "목", "금", "토" ],
						dateFormat: 'yy년 mm월 dd일',
						prevText: "이전 달",
						nextText: "다음 달",
						showOn: "both",
						buttonText: "달력보기",
					});

					$(".startDate").on("change", function(e) {
						var startDate = $(".startDate").val();
						var emsDate = $(this).hasClass("emsDate");
						var patternEng = /[\D]/;
						var datePattern = /^(19|20)\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[0-1])$/;
						var formatStartdate= $.datepicker.formatDate('yymmdd ', $(".startDate").datepicker('getDate'));
						var formatEnddate= $.datepicker.formatDate('yymmdd ', $(".endDate").datepicker('getDate'));
						var datePickerPattern = /^(19|20)\d{2}년 (0[1-9]|1[012])월 (0[1-9]|[12][0-9]|3[0-1])일$/;

						if((!datePattern.test(startDate) && !datePickerPattern.test(startDate)) || e.keyCode == 13 ) {
							openAlertModal("알림", "시간 입력이 잘못 되었습니다. \n 확인해주세요. ex)20180101", null , null);
							return false;
						}

						if(startDate.length == 8 && !patternEng.test(startDate)) {
							 var y = startDate.substr(0, 4);
							 var m = startDate.substr(4, 2);
							 var d = startDate.substr(6, 2);

							 var changeRealDate = y + "년 " + m + "월 " + d + "일";
							 formatStartdate = startDate;
							 $(".startDate").val(changeRealDate);
						}

						if(formatStartdate != ""&& formatEnddate != "") {
							if((formatStartdate > formatEnddate)) {
								if(emsDate) {
									openAlertModal("알림", "시간 선택이 잘못 되었습니다.", function(){setCorrectDate('startDate');} , null);
								} else {
									openAlertModal("알림", "시간 선택이 잘못 되었습니다.", null , null);
								}
								return false;
							}
						}
					});

					$(".endDate").on("change", function(e) {

						var endDate = $(".endDate").val();
						var emsDate = $(this).hasClass("emsDate");
						var patternEng = /[\D]/;
						var datePattern = /^(19|20)\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[0-1])$/;
						var formatStartdate= $.datepicker.formatDate('yymmdd ', $(".startDate").datepicker('getDate'));
						var formatEnddate= $.datepicker.formatDate('yymmdd ', $(".endDate").datepicker('getDate'));
						var datePickerPattern = /^(19|20)\d{2}년 (0[1-9]|1[012])월 (0[1-9]|[12][0-9]|3[0-1])일$/;

						if((!datePattern.test(endDate) && !datePickerPattern.test(endDate)) || e.keyCode == 13 ) {
							openAlertModal("알림", "시간 입력이 잘못 되었습니다. \n 확인해주세요. ex)20180101", null , null);
							return false;
						}

						if(endDate.length == 8 && !patternEng.test(endDate)) {
								 var y = endDate.substr(0, 4);
								 var m = endDate.substr(4, 2);
								 var d = endDate.substr(6, 2);

								 var changeRealDate = y + "년 " + m + "월 " + d + "일";
								 formatEnddate = endDate;
								 $(".endDate").val(changeRealDate);
						}

						if(formatStartdate != ""&& formatEnddate != "") {
							if(formatStartdate > formatEnddate) {
								if(emsDate) {
									openAlertModal("알림", "시간 선택이 잘못 되었습니다.", function(){setCorrectDate('endDate');} , null);
								} else {
									openAlertModal("알림", "시간 선택이 잘못 되었습니다.", null , null);
								}
								return false;
							}
						}

					});
				},
				getTaskMsg:function(bindKey, webTid) {
					$.ajax({
						type:"POST",
						url:"/task/getTaskMsgListAjax",
						traditional:true,
						data:{
							bindKey:bindKey,
							webTid:webTid
						},
						success:function(data) {
							return data.data;
						},
						error:function(xhr) {
							console.log(xhr);
						}
					});
					/*setInterval(function () {
						$.ajax({
							type: "POST",
							url: "/task/getTaskMsgListAjax",
							traditional: true,
							data: {bindKey : bindKey, webTid : webTid},
							success: function (data) {
								console.log(data.data);
								return data.data;

							},
							error: function (xhr) {
								console.log(xhr);
							}
						});
					}, time);*/
				},
				// 화면 전환 전 반드시 호출
				// menuId의 경우 메뉴 하이라이트가 변경될 케이스에만 해당 메뉴ID를 입력
				gnbSetMenu: function(menuId) {
					if (menuId === undefined || menuId == null) {
						menuId = "";
					}
					window.parent.postMessage({"type": "gnbSetMenu", "menuId": menuId}, "*");   // '*' on any domain
				}
			};
		})();

function monitoringDashboardPop(selector, name, resultData){

    if(name == 'dashboardDetail'){
        $("#alarm_detail_fault_detail").text("");
        if(resultData.perceivedSeverity == 6 || resultData.perceivedSeverity == "6") {
            if (resultData.faultDetails != null){
                if (resultData.extifType != 'E2EO') { // E2EO(내부알람)의 경우 Detail값 JSON 아님
                    $("#alarm_detail_fault_detail").append(JSON.stringify(JSON.parse(resultData.faultDetails), null, "  "));
                } else {
                    $("#alarm_detail_fault_detail").text(resultData.faultDetails);
                }
            }
        } else {
            $("#alarm_detail_fault_detail").append(
                resultData.faultDetails.replace(/\\n/gi, "<br/>")
            );
        }

        $("#alarm_detail_timestamp").text(resultData.timeStamp);
        $("#alarm_detail_instance_name").text(resultData.objInsName);
        $("#alarm_detail_noti_type").text(resultData.notiType);
        $("#alarm_detail_probable_cause").text(resultData.probableCause);
        $("#alarm_detail_is_root_cause").text(resultData.isRootCause);
        $("#alarm_detail_raised_time").text(resultData.alarmRaisedTime);
        $("#alarm_detail_perceived_severity").text(resultData.perceivedSeverityNm);
        $("#alarm_detail_event_type").text(resultData.eventTypeNm);

        if(resultData.alarmClearedTime !== undefined && resultData.alarmClearedTime != null && resultData.alarmClearedTime != ""){
            $("#alarm_detail_creared_time").text(resultData.alarmClearedTime);
        }else{
            //	E2E 에서 확인 버튼을 누른 알람인지 확인
            if(resultData.confirmFlag !== undefined && resultData.confirmFlag != null && resultData.confirmFlag != "" && resultData.confirmFlag.toUpperCase() == "Y"){
                // 확인 버튼 눌렀을 때 시간이 들어 있는지 확인
                if(resultData.userConfirmTime !== undefined && resultData.userConfirmTime != null && resultData.userConfirmTime != ""){
                    // 확인 버튼 누른 유저가 있는지 확인
                    if(resultData.confirmUser !== undefined && resultData.confirmUser != null && resultData.confirmUser != "" && resultData.confirmUser != "-"){
                        var alarmConfirmTimeAndUser = "" + resultData.userConfirmTime + " ( " + resultData.confirmUser + " )";
                        $("#alarm_detail_creared_time").text(alarmConfirmTimeAndUser);
                    }else{	// 확인 버튼 유저가 조회가 안될 때
                        $("#alarm_detail_creared_time").text(resultData.confirmUser);
                    }
                }
            }
        }
    }else {
        if(name == 'dashboardMask'){
            if(resultData == null || resultData == undefined) {
                openAlertModal("Mask 상세보기", "Mask 된 정보가 없습니다.", null, null);
                return;
            }
            $(".recentUserTitle").text("Mask 실행자");
            $(".maskTimeTitle").text("Mask 실행 시간");
            $("#title_maskConfirm").text("Mask 상세보기");
        }else if(name == 'dashboardCheck'){
            $(".recentUserTitle").text("확인자");
            $(".maskTimeTitle").text("확인 시간");
            $("#title_maskConfirm").text("확인 상세보기");
        }
        $(".recentUser").text(resultData.confirmUser);
        $(".maskTime").text(resultData.userConfirmTime);
        $(".description").text(resultData.confirmDesc);
    }

    e2eo.openPopup(selector);

}

var setCorrectDate = function(target) {
	$('.'+target).datepicker('setDate', 'today');
};
