/**
 * SDNO Domain Tosca
 * @type {SDNODomainTosca}
 */
var SDNODomainTosca = (function() {

    function SDNOTosca(data, isDomain) {
        combineComponentAndProfile(data.components, data.nsdfs, data.vlds);

        setCompAssociation(data.components);

        var template = isDomain ? generateDomainTemplate(data)
                                : generateE2eTemplate(data);

        Object.defineProperties(this, {
            template: {
                get: function() {
                    return template;
                }
            }
        })
    }

    SDNOTosca.prototype = {
        /**
         * return YAML data
         * @returns {string}
         */
        getYAML: function() {
            var yamlConverter = new YamlConverter(this.template, true);

            return yamlConverter.convert();
        }
    };

    function arrayToHashById(dataList) {
        var ret = {};

        dataList.forEach(function(data) {
            ret[data.id] = data;
        });

        return ret;
    }

    function combineComponentAndProfile(components, nsdfs, vlds) {
        var componentHash = arrayToHashById(components);

        nsdfs.forEach(function(nsdf) {
            if (nsdf.vnfProfile) {
                nsdf.vnfProfile.forEach(function (vnf) {
                    var component = componentHash[vnf.id];

                    if (component != null) {
                        component.__profile = vnf;
                    }
                });
            }

            if (nsdf.nsProfile) {
                nsdf.nsProfile.forEach(function (ns) {
                    var component = componentHash[ns.id];

                    if (component != null) {
                        component.__profile = ns;
                    }
                });
            }

            nsdf.virtualLinkProfile.forEach(function(virtual) {
                var component = componentHash[virtual.id];

                if (component != null) {
                    if (component.__profile == null) {
                        component.__profile = {};
                    }

                    component.__profile.info = virtual;
                }
            });
        });

        if (Array.isArray(vlds)) {
            vlds.forEach(function (vld) {
                var component = componentHash[vld.id];

                if (component != null) {
                    if (component.__profile == null) {
                        component.__profile = {};
                    }

                    component.__profile.connectivityType = vld.connectivityType;
                }
            });
        }
    }

    function setCompAssociation(allComps) {
        var components = {},
            connections = [];

        allComps.forEach(function(comp) {
            if (comp.type === 'draw2d.Connection') {
                connections.push(comp);
            } else {
                components[comp.id] = comp;
            }
        });

        connections.forEach(function(conn) {
            var src = conn.source,
                dst = conn.target,
                srcComp = components[src.node],
                dstComp = components[dst.node],
                srcCompType = Number(srcComp.userData.type),
                dstCompType = Number(dstComp.userData.type);

            if (srcCompType === 4 && (dstCompType < 2 || dstCompType === 5)) {
                if (!dstComp.__requirements) {
                    dstComp.__requirements = {};
                }

                dstComp.__requirements[srcComp.id] = [srcComp.label.text, conn];
            }

            if (dstCompType === 4 && (srcCompType < 2 || srcCompType === 5)) {
                if (!srcComp.__requirements) {
                    srcComp.__requirements = {};
                }

                srcComp.__requirements[dstComp.id] = [dstComp.label.text, conn];
            }
        });
    }

    function generateDomainTemplate(data) {
        var generalInfo = data.generalInfo;

        var templateObj = {
            tosca_definitions_version: 'tosca_simple_yaml_1_2',
            description: generalInfo.nsdDesc,
            imports: [
                'etsi_nfv_sol001_vnfd_2_5_1_types_.yaml',
                'etsi_nfv_sol001_nsd_2_5_1_types_.yaml'
            ],
            node_types: {},
            topology_template: {
                substitution_mappings: {
                    node_types: 'tosca.nodes.nfv.' + generalInfo.nsdName,
                    requirements: (function() {
                        var ret = {};

                        data.components.forEach(function(component) {
                            if (Number(component.userData.type) === 5) {
                                var sapProfileName = component.label.text;

                                ret['virtual_link_' + sapProfileName] = '[' + sapProfileName + ', external_virtual_link]';
                            }
                        });

                        return ret;
                    })()
                },
                node_templates: Object.assign((function() {
                    var nsdObj = {};

                    nsdObj[generalInfo.nsdName] = {
                        type: 'tosca.nodes.nfv.' + generalInfo.nsdName,
                        properties: {
                            descriptor_id: generalInfo.nsdId,
                            designer: generalInfo.designer,
                            name: generalInfo.nsdName,
                            version: generalInfo.version,
                            invariant_id: generalInfo.nsdInvariantId,
                            flavour_id: 'simple'
                        }
                    };

                    return nsdObj;
                })(), (function(components) {
                    var ret = {};

                    for (var i = 0; i < components.length; i++) {
                        var component = components[i],
                            userData = component.userData,
                            componentType = Number(userData.type),
                            obj = null;

                        if (componentType <= 1) {
                            obj = getVNFTemplate();

                            var props = obj.properties;

                            props.descriptor_id = userData.id;
                            props.flavour_id = 'simple';

                            var componentProfile = component.__profile;

                            if (componentProfile) {
                                obj.requirements = (function(connectivities) {
                                    var r = [];

                                    if (!connectivities) {
                                        return [];
                                    }

                                    connectivities.forEach(function(conn) {
                                        r.push({ virtual_link: conn.virtualLinkProfileId });
                                    });

                                    return r;
                                })(componentProfile.nsVirtualLinkConnectivity);
                            }
                        } else if (componentType === 5) {
                            obj = getSAPTemplate();
                            var sapObj = {},
                                requirements = component.__requirements;

                            sapObj['internal_virtual_link'] =  requirements ? Object.values(requirements)[0][0] : '';
                            sapObj['external_virtual_link'] =  '';

                            obj.requirements = sapObj;
                        } else if (componentType === 4) {
                            obj = getVLTemplate();

                            var props = obj.properties;
                            var componentProfile = component.__profile;

                            if (componentProfile) {
                                if (componentProfile.connectivityType) {
                                    props.connectivity_type.layer_protocols = '[ ' + componentProfile.connectivityType.layerProtocol + ' ]';
                                    props.connectivity_type.flow_pattern = componentProfile.connectivityType.flowPattern;
                                }

                                if (componentProfile.info) {
                                    props.vl_profile.max_bitrate_requirements.root = componentProfile.info.maxBitrateRequirements.root;
                                    props.vl_profile.min_bitrate_requirements.root = componentProfile.info.minBitrateRequirements.root;
                                }
                            }
                        }

                        if (obj != null) {
                            ret[component.label.text] = obj;
                        }
                    }

                    return ret;
                })(data.components))
            }
        };

        templateObj.node_types['tosca.nodes.nfv.' + generalInfo.nsdName] = {
            derived_from: 'tosca.nodes.nfv.NS'
        };

        return templateObj;
    }

    function generateE2eTemplate(data) {
        var generalInfo = data.generalInfo;

        return {
            tosca_definitions_version: 'tosca_simple_yaml_1_2',
            description: generalInfo.nsdDesc,
            imports: (function(components) {
                var ret = [];

                components.forEach(function(component) {
                    var userData = component.userData,
                        componentType = Number(userData.type);

                    if (componentType <= 1) {
                        ret.push('NSD:' + component.label.text);
                    }
                });

                return ret;
            })(data.components),
            topology_template: {
                node_templates: Object.assign((function() {
                    var nsdObj = {};

                    nsdObj[generalInfo.nsdName] = {
                        type: 'tosca.nodes.nfv.' + generalInfo.nsdName,
                        properties: {
                            descriptor_id: generalInfo.nsdId,
                            designer: generalInfo.designer,
                            name: generalInfo.nsdName,
                            version: generalInfo.version,
                            invariant_id: generalInfo.nsdInvariantId,
                            flavour_id: 'simple'
                        }
                    };

                    return nsdObj;
                })(), (function(components) {
                    var ret = {},
                        componentHash = arrayToHashById(components),
                        i = 0,
                        component,
                        userData,
                        componentType,
                        obj;

                    for (i = 0; i < components.length; i++) {
                        component = components[i];

                        if (component.type === 'draw2d.Connection') {
                            var src = component.source,
                                dst = component.target;

                            if (/sap/gi.test(src.port)) {
                                if (!componentHash[src.node].__sapInfo) {
                                    componentHash[src.node].__sapInfo = [];
                                }

                                componentHash[src.node].__sapInfo.push(src.port.split('.')[0] + '|' + componentHash[dst.node].label.text);
                            } else if (/sap/gi.test(dst.port)) {
                                if (!componentHash[dst.node].__sapInfo) {
                                    componentHash[dst.node].__sapInfo = [];
                                }

                                componentHash[dst.node].__sapInfo.push(dst.port.split('.')[0] + '|' + componentHash[src.node].label.text);
                            }
                        }
                    }

                    for (i = 0; i < components.length; i++) {
                        component = components[i];
                        userData = component.userData;
                        componentType = Number(userData.type);
                        obj = {};

                        if (componentType === 4) {
                            obj = {
                                type: 'tosca.nodes.nfv.NsVirtualLink'
                            };

                            ret[component.label.text] = obj;
                        }
                    }

                    for (i = 0; i < components.length; i++) {
                        component = components[i];
                        userData = component.userData;
                        componentType = Number(userData.type);
                        obj = {};

                        // Nsd
                        if (componentType <= 1) {
                            var componentProfile = component.__profile,
                                nsdRequirements = component.__sapInfo;

                            obj = {
                                type: 'tosca.nodes.nfv.' + component.label.text
                            };

                            if (nsdRequirements) {
                                obj.requirement = (function() {
                                    var r = [];

                                    nsdRequirements.forEach(function(req) {
                                        var info = req.split('|'), obj = {};

                                        obj['virtual_link_' + info[0]] = info[1];

                                        r.push(obj);
                                    });

                                    return r;
                                })();
                            }

                            ret[component.label.text] = obj;
                        }
                    }

                    return ret;
                })(data.components))
            }
        };
    }

    function getSAPTemplate() {
        return {
            type: 'tosca.nodes.nfv.Sap',
            properties: '',
            requirements: ''
        };
    }

    function getVNFTemplate() {
        return {
            type: 'tosca.nodes.nfv.VNF',
            properties: {
                flavour_id: ''
            },
            requirements: ''
        };
    }

    function getVLTemplate() {
        return {
            type: 'tosca.nodes.nfv.NsVirtualLink',
            properties: {
                connectivity_type: {
                    layer_protocols: '',
                    flow_pattern: ''
                },
                vl_profile: {
                    max_bitrate_requirements: {
                        root: ''
                    },
                    min_bitrate_requirements: {
                        root: ''
                    }
                }
            }
        };
    }

    return SDNOTosca;
})();