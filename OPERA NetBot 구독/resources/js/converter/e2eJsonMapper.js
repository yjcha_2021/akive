/**
 * Json 템플릿에 Json 데이터 값 맵핑
 * @since 2018-12-12
 * @version 0.0.1
 * @author kangaroo
 */
var JsonMapper = (function() {

    /**
     * Json Mapper Util
     */
    var __ = (function() {

        /**
         * @param {object} data
         * @constructor
         */
        function __(data) {
            this.rootData = data;
        }

        /**
         * @method
         */
        __.prototype = {
            /**
             * 슬롯 정규식
             * @return {RegExp} regexp
             * */
            slotRegexp: function() {
                return /{(:|each|if|loop)\s*(\S+)\s*(eq|ne|of)?\s*(\S+)?\s*}/g;
            },
            /**
             * 키 값 판별 정규식
             * @return {RegExp} regexp
             * */
            nestRegexp: function() {
                return /^([a-zA-Z$_][a-zA-Z0-9$_]*)(\.[a-zA-Z$_][a-zA-Z0-9$_]*)*$/g;
            },
            /**
             * 비교대상이 변수인지 단순 스트링인지 판별 정규식
             * @return {RegExp} regexp
             */
            stringRegexp: function() {
                return /^"(\S*)"$|^'(\S*)'$/g;
            },
            /**
             * Input이 Json.parse 함수를 사용하는지 판별하는 정규식
             * @return {RegExp} regexp
             */
            jsonParserRegexp: function() {
                return /JSON\.parse\((\S*)\)(\.(\S*))*/g;
            },
            /**
             * Property를 root 데이터부터 찾을지 판별하는 정규식
             * @return {RegExp} regexp
             */
            rootRegexp: function() {
                return /^\$root(\.[a-zA-Z$_][a-zA-Z0-9$_]*)+$/g
            },
            isEmptyArray: function(arr) {
                return (Array.isArray(arr) && arr.length === 0);
            },
            isEmptyObj: function(o) {
                return (typeof o === 'object' && Object.keys(o).length === 0);
            },
            /**
             * Property를 root 데이터부터 찾을지 판별 여부
             * @param {string} s
             * @return {boolean} boolean
             */
            isFromRoot: function(s) {
                return this.rootRegexp().test(s);
            },
            /**
             * Input이 Json.parse 함수를 사용하는지 판별 여부
             * @param {string} s
             * @return {boolean} boolean1
             */
            isJsonParser: function(s) {
                return this.jsonParserRegexp().test(s);
            },
            /**
             * 비교대상이 변수인지 단순 스트링인지 판별 여부
             * @param {string} s
             * @return {boolean} boolean
             */
            isStringParm: function(s) {
                return this.stringRegexp().test(s);
            },
            /**
             * 슬롯 판별 여부
             * @param {string} s String value
             * @return {boolean} boolean
             * */
            isSlot: function(s) {
                return this.slotRegexp().test(s);
            },
            /**
             * 슬롯에 있는 프로퍼티가 nested인지 판별 여부
             * @param {string} p String property
             * @return {boolean} boolean
             */
            isNestedProperty: function(p) {
                return this.nestRegexp().test(p);
            },
            /**
             * sliceObject를 사용하여 분할된 Object를 다시 Object로 합침
             * @see __.sliceObject
             * @param {Array} arr Json 템플릿이 분할 된 Array [{k: k, v: v}] 포멧
             * @return {object|Array} object
             */
            arrToObj: function(arr) {
                var obj = {};

                arr.forEach(function(v) {
                    (typeof v === 'object') && (obj[v.k] = v.v);
                });

                return Object.keys(obj).length > 0 ? obj : arr;
            },
            /**
             * 템플릿 Object를 순서 유지를 위해서 Array 형식으로 변형
             * @param {object} o Json 템플릿 (오브젝트 형식)
             * @return {Array<{k: string, v: object}>} array
             */
            sliceObject: function(o) {
                var arr = [],
                    keys = Object.keys(o);

                keys.forEach(function(k) {
                    arr.push({k: k, v: o[k]});
                });

                return arr;
            },
            getValueFromFn: function(j, d) {
                var context = this,
                    fn = context.jsonParserRegexp().exec(j),
                    param = fn[1],
                    props = fn[3],
                    value = this.isStringParm(param) ? JSON.parse(param)
                                                     : JSON.parse(this.getValueFromData(param, d) + '');

                if (props != null) {
                    value = this.getValueFromData(props, value);
                }

                return value;
            },
            /**
             * Get value of data
             * @param {string} p String property
             * @param {object} d object data
             * @return {any} any
             */
            getValueFromData: function(p, d) {
                var context = this,
                    data = d;

                if (context.isFromRoot(p)) {
                    p = context.rootRegexp().exec(p)[1];
                    data = context.rootData;
                }

                if (context.isNestedProperty(p)) {
                    p.split('.').forEach(function(pn) {
                        if (data == null) {
                            return 'null';
                        }

                        data = data[pn];
                    });
                } else {
                    data = d[p];
                }

                return data;
            },
            /**
             * Get slot info
             * @param {string} s
             * @return {object} object
             */
            getSlotInfo: function(s) {
                var slotInfo = this.slotRegexp().exec(s);

                return {
                    indicator: slotInfo[1],
                    val: slotInfo[2],
                    exp: slotInfo[3],
                    val2: slotInfo[4]
                };
            },
            /**
             * 템플릿에 데이터를 맵핑 Array type
             * @param {Array} template
             * @param {object} data
             * @return {Array} Array
             */
            arrayMapper: function(template, data) {
                var context = this;

                for (var idx = 0; idx < template.length; idx++) {
                    var item = template[idx];

                    if (Array.isArray(item)) {
                        item = context.arrayMapper(item, data);
                    } else if (typeof item === 'object') {
                        item = context.arrToObj(context.objectMapper(item, data));
                    } else if (context.isSlot(item)) {
                        var slotInfo = context.getSlotInfo(item),
                            bindedValue = context.isJsonParser(slotInfo.val) ? context.getValueFromFn(slotInfo.val, data)
                                                                             : context.getValueFromData(slotInfo.val, data);

                        if (Array.isArray(bindedValue)) {
                            item = context.arrayMapper(item, data);
                        } else if (typeof bindedValue === 'object') {
                            item = context.arrToObj(context.objectMapper(bindedValue, data));
                        } else {
                            item = bindedValue;
                        }
                    }

                    template[idx] = item;
                }

                return template;
            },
            /**
             * 템플릿에 데이터를 맵핑 Object type
             * @param {Array<{k: string, v: object}>} template
             * @param {object} data
             * @param parent
             * @return {Array} Array
             */
            objectMapper: function(template, data, parent) {
                var context = this,
                    slicedTemplate = context.sliceObject(template);

                for (var idx = 0; idx < slicedTemplate.length; idx++) {
                    var item = slicedTemplate[idx],
                        key = item.k,
                        value = item.v;

                    /**
                     * case key값이 슬롯일 경우
                     */
                    if (context.isSlot(key)) {
                        var slotInfo = context.getSlotInfo(key),
                            bindedKey = context.isJsonParser(slotInfo.val) ? context.getValueFromFn(slotInfo.val, data)
                                                                           : context.getValueFromData(slotInfo.val, data);

                        /**
                         * case key값이 null|undefined 일경우 제외
                         */
                        if (bindedKey == null) {
                            slicedTemplate.splice(idx--, 1);
                            continue;
                        }

                        /**
                         * case 1-1: key값이 슬롯이고 단순 데이터 맵핑일 경우
                         */
                        if (slotInfo.indicator === ':') {
                            item.k = bindedKey;

                            /**
                             * case 1-3: key값이 슬롯이고 if, each등 expression이 들어간 맵핑일 경우
                             */
                        } else if (slotInfo.indicator === 'if') {
                            var condition,
                                bindedKey2;

                            if (context.isStringParm(slotInfo.val2)) {
                                bindedKey2 = slotInfo.val2.replace(/['|"]/g, '');
                            } else {
                                bindedKey2 = context.getValueFromData(slotInfo.val2, data);
                            }

                            if (slotInfo.exp === 'eq') {
                                if (slotInfo.val2  === 'null') {
                                    condition = context.isEmptyObj(bindedKey);
                                } else {
                                    condition = (bindedKey == bindedKey2);
                                }
                            } else if (slotInfo.exp === 'ne') {
                                if (slotInfo.val2  === 'null') {
                                    condition = !context.isEmptyObj(bindedKey);
                                } else {
                                    condition = (bindedKey != bindedKey2);
                                }
                            }

                            if (condition) {
                                slicedTemplate.splice(idx, 1);

                                context.sliceObject(value).forEach(function(cv, j) {
                                    slicedTemplate.splice(idx + j, 0, cv);
                                });

                                idx--;
                            } else {
                                slicedTemplate.splice(idx--, 1);
                            }

                            continue;
                        } else {
                            /**
                             * case 1-2-1: Slot type이 each|loop일 경우
                             */
                            if (Array.isArray(bindedKey)) {
                                // each
                                if (slotInfo.indicator === 'each') {
                                    slicedTemplate.splice(idx, 1);

                                    bindedKey.forEach(function(bindData, j) {
                                        var bindResult = context.objectMapper(value, bindData)[0];

                                        bindResult != null && slicedTemplate.splice(idx + j, 0, {k: bindResult.k + '_' +j, v: bindResult.v});
                                    });

                                    continue;
                                    // loop
                                } else if (slotInfo.indicator === 'loop') {
                                    var retArr = [];

                                    if (typeof value === 'object') {
                                        bindedKey.forEach(function(v) {
                                            var c1 = context.objectMapper(JSON.parse(JSON.stringify(value)), v),
                                                c2 = context.arrToObj(c1);

                                            (!Array.isArray(c2)) && retArr.push(c2);
                                        });

                                        parent.v = retArr;
                                    }

                                    return true;
                                }
                            }
                        }
                    }

                    /**
                     * case value 값이 슬롯일 경우
                     */
                    if (Array.isArray(value)) {
                        item.v = context.arrayMapper(value, data);
                    } else if (typeof value === 'object') {
                        var objRet = context.objectMapper(value, data, slicedTemplate[idx]);

                        if (typeof objRet === 'boolean' && objRet) {
                            continue;
                        } else {
                            item.v = context.arrToObj(objRet);
                        }
                    } else if (context.isSlot(value)) {
                        var slotInfo = context.getSlotInfo(value),
                            slotValue = slotInfo.val,
                            bindedValue = context.isJsonParser(slotValue) ? context.getValueFromFn(slotValue, data)
                                                                          : context.getValueFromData(slotValue, data);

                        /**
                         * case value에 맵핑할 데이터 타입이 Array일 경우
                         */
                        if (bindedValue == null) {
                            item.v = 'null';
                        } else if (Array.isArray(bindedValue)) {
                            item.v = context.arrToObj(context.arrayMapper(bindedValue, data));
                            /**
                             * case value에 맵핑할 데이터 타입이 Object일 경우
                             */
                        } else if (typeof bindedValue === 'object') {
                            item.v = context.arrToObj(context.objectMapper(bindedValue, data));

                            /**
                             * case value에 맵필할 데이터 타입이 일반 타입일 경우
                             */
                        } else {
                            item.v = bindedValue;
                        }
                    }

                    /**
                     * 변경값 재 할당
                     */
                    slicedTemplate[idx] = item;
                }

                return slicedTemplate;
            }
        };

        return __;
    })();

    /**
     * @param template Json 템플릿
     * @param data Json 템플릿에 맵핑 할 데이터
     * @constructor
     * */
    function JsonMapper(template, data) {
        this.t = template;
        this.d = dataFilter(data);
    }

    /**
     * Json 템플릿에 데이터 맵핑
     * @return {object} object
     * */
    JsonMapper.prototype.process = function() {
        /**
         * --- case 1: template 형태가 Array일 경우 (보류)
         * case 2: template 형태가 Json Object인 경우
         */
        var template = this.t,
            data = this.d,
            helper = new __(data);

        var obj = helper.objectMapper(template, data);

        return helper.arrToObj(obj);
    };

    function dataFilter(data) {
        var components = data.components,
            generalInfo = data.generalInfo,
            compObj = {},
            connections = [],
            i;

        for (i = 0; i < components.length; i++) {
            var comp = components[i];

            if (comp.type === 'draw2d.Connection') {
                connections.push(comp);
            } else {
                if (!comp.requirements) {
                    comp.requirements = [];
                }

                compObj[comp.id] = comp;
            }
        }

        for (i = 0; i < connections.length; i++) {
            var conn = connections[i],
                src = compObj[conn.source.node],
                target = compObj[conn.target.node];

            if (Number(src.userData.type) !== 4) {
                src.requirements.push({virtual_link: target.label.text});
            } else {
                target.requirements.push({virtual_link: src.label.text});
            }
        }

        return {
            components: (function(o) {
                var keys = Object.keys(o),
                    ret = [];

                for (var i = 0; i < keys.length; i++) {
                    var k = keys[i],
                        comp = o[k];

                    ret.push(comp);
                }

                return ret;
            })(compObj),
            generalInfo: generalInfo
        };
    }

    return JsonMapper;
})();