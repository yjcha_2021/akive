var YamlConverter = (function() {

	var spacing = "  ";

	/**
	 * @param o
	 * @constructor
	 */
	function YamlConverter(o, unquoto) {
		this.o = o;
		this.s = [];
		this.unquoto = unquoto;
	}

	/**
	 * @method
	 */
	YamlConverter.prototype = {
		convert: function() {
			var context = this,
				obj = context.o,
				ret = [];

			convert.call(this, obj, ret);

			return ret.join('\n');
		},
		addToString: function(/*args*/) {
			var context = this;

			for (var i = 0; i < arguments.length; i++) {
				var arg = arguments[i];

				if (typeof arg === 'string') {
					context.s.push(arg);
				}
			}

			return this;
		}
	};

	/******************************
	 *      Private Fields        *
	 ******************************/

	function escapeReg() {
		return /[\[|\]|{|}|>|\||\*|&|!|%|#|`|@|,|\?|:|\-]/g;
	}

	function getType(obj) {
		if (obj == null) {
			return 'null';
		} else if (typeof obj === 'object') {
			if (Array.isArray(obj)) {
				return 'array';
			} else {
				return 'hash';
			}
		} else {
			return 'plain';
		}
	}

	function normalize(str, unquoto) {
		if (!unquoto) {
			if (escapeReg().test(str)) {
				return "\"" + str + "\"";
			}
		}

		return str;
	}

	// function arrayToString(o) {
	// 	var ret = [];
	//
	// 	for (var i = 0; i < o.length; i++) {
	// 		var item = o[i];
	//
	// 		if (typeof item === 'object') {
	// 			if (Array.isArray(item)) {
	// 				ret.push(arrayToString(item));
	// 			} else {
	// 				ret.push(objectToString(item));
	// 			}
	// 		} else {
	// 			ret.push(normalize(o.toString()));
	// 		}
	// 	}
	//
	// 	return '[ ' + ret.join(', ') + ' ]';
	// }

	// function objectToString(o) {
	// 	var keys = Object.keys(o),
	// 		ret = [];
	//
	// 	for (var i = 0, len = keys.length; i < len; i++) {
	// 		var k = keys[i],
	// 			v = o[k];
	//
	// 		ret.push(k + ': ' + v);
	// 	}
	//
	// 	return ret.join(', ');
	// }

	function convert(obj, ret) {
		var type = getType(obj);

		switch (type) {
			case 'array':
				convertArray.call(this, obj, ret);
				break;
			case 'hash':
				convertHash.call(this, obj, ret);
				break;
			case 'null':
				ret.push('null');
				break;
			case 'plain':
				ret.push(normalize(obj.toString(), this.unquoto));
		}
	}

	function convertArray(obj, ret) {
		if (obj.length === 0) {
			ret.push('[]');
		}

		for (var i = 0; i < obj.length; i++) {
			var item = obj[i],
				recurse = [];

			convert.call(this, item, recurse);

			for (var j = 0; j < recurse.length; j++) {
				ret.push((j === 0 ? "- " : spacing) + recurse[j]);
			}
		}
	}

	function convertHash(obj, ret) {
		var context = this,
			keys = Object.keys(obj);

		if (keys.length === 0) {
			ret.push('{}');
			return;
		}

		for (var i = 0, len = keys.length; i < len; i++) {
			var k = keys[i],
			    v = obj[k],
			    rec = [],
				type = getType(v);

			if (
				(type === 'hash' && Object.keys(v).length === 0) ||
				(type === 'array' && v.length === 0)
			) {
				type = 'plain';
			}

			convert.call(context, v, rec);

			if (type === 'plain' || type === 'null') {
				ret.push(k + ': ' + rec[0]);
			} else {
				ret.push(k + ': ');

				for (var j = 0; j < rec.length; j++) {
					ret.push(spacing + rec[j]);
				}
			}
		}
	}

	return YamlConverter;
})();