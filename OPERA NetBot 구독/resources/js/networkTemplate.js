function getNetworkInfoTemplate(profileId) {
	if (profileId == 10) {
		return {
			"sgi": {
				"ip": [
					{
						"end": "",
						"type": "IPV4",
						"count": "",
						"start": ""
					}
				],
				"vr": "",
				"startvlan": ""
			},
			"command": "10"
		};
	} else if (profileId == 11) {
		return {
			"gtpu": {
				"ip": [
					{
						"end": "",
						"type": "IPV4",
						"count": "1",
						"start": ""
					}
				],
				"vr": "",
				"startvlan": ""
			},
			"command": "11"
		};
	} else if (profileId == 12) {
		return {
			"reip": [
				{
					"ip": {
						"end": "",
						"type": "IPV4",
						"count": "1",
						"start": ""
					},
					"vr": "",
					"type": "PFCP"
				}
			],
			"command": "12"
		};
	} else if (profileId == 13) {
		return {
			"gtpu": {
				"ip": [
					{
						"end": "",
						"type": "IPV4",
						"count": "1",
						"start": ""
					}
				],
				"vr": "",
				"startvlan": ""
			},
			"command": "13"
		};
	} else if (profileId == 14) {
		return {
			"upf": [
				{
					"ip": {
						"end": "",
						"grp": "",
						"type": "IPV4",
						"count": "1",
						"start": ""
					},
					"vr": "",
					"peer": "PEER-PGW"
				},
				{
					"ip": {
						"end": "",
						"grp": "",
						"type": "IPV4",
						"count": "1",
						"start": ""
					},
					"vr": "",
					"peer": "PEER-SGW"
				}
			],
			"command": "14"
		};
	}
	return "";

}